from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from barter_app.common import NgWord# NGワード
from django.core.validators import MinLengthValidator

class Prefectures2(models.Model):
    """都道府県モデル"""
    
    code = models.IntegerField(verbose_name='都道府県コード', primary_key=True)
    name = models.CharField(verbose_name='都道府県名', max_length=30,null=True,blank=True)

    class Meta:
        verbose_name_plural = 'Prefectures2'
    
    def __str__(self):
        return self.name

class CustomUser(AbstractUser):

    class Meta:
        verbose_name_plural = 'CustomUser'
    
    # ユーザーアイコン
    usericon = models.ImageField(verbose_name='ユーザーアイコン', blank=True, null=True)
    # ユーザー名
    username = models.CharField(verbose_name='ユーザー名', validators=[NgWord], max_length=15, blank=True, null=True)
    # 生年月日
    birthday_regex = RegexValidator(regex=r'[12]\d{3}-[01][0123456789]-\d{2}', message = ("※入力内容をご確認ください。"))
    birthday = models.CharField(verbose_name='生年月日', validators=[birthday_regex], max_length=10, blank=True, null=True)
    # メールアドレス
    email = models.EmailField(verbose_name='メールアドレス', max_length=30, blank=True, null=True)
    # 電話番号
    tel_number_regex = RegexValidator(regex=r'^[0-9]+$', message = ("※入力内容をご確認ください。"))
    telephone = models.CharField(verbose_name='電話番号', validators=[tel_number_regex, MinLengthValidator(10)], max_length=11, default="")
    # 都道府県
    prefecture = models.ForeignKey(Prefectures2, verbose_name='都道府県',max_length=5, on_delete=models.PROTECT, blank=True, null=True)
    # 住所
    address = models.CharField(verbose_name='住所(市区町村・番地)', validators=[NgWord], max_length=40, blank=True, null=True)
    # 住所2
    address2 = models.CharField(verbose_name='住所2(建物名)', validators=[NgWord], max_length=40, blank=True, null=True)
    # 郵便番号
    postalcode_regex = RegexValidator(regex=r'^[0-9]+$', message = ("※入力内容をご確認ください。"))
    zip_code = models.CharField(verbose_name='郵便番号', validators=[postalcode_regex, MinLengthValidator(7)], max_length=7, blank=True, null=True)
    # 希望商品
    comment = models.TextField(verbose_name='希望商品：あなたが欲しい商品を記入して下さい。', validators=[NgWord], max_length=100, blank=True, null=True)

    # チケット
    ticket = models.IntegerField(verbose_name='チケット枚数', default=0, null=True)

    #ログイン判定
    login_flg = models.BooleanField(verbose_name='初回ログインはTrue', default='True', null=True)
    #良い評価
    good_evaluation = models.IntegerField(verbose_name='良かった評価', default=0, null=True)
    #悪い評価
    bad_evaluation = models.IntegerField(verbose_name='悪かった評価', default=0, null=True)

    
    def __str__(self):
        return self.username