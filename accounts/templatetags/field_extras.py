from django import template
from accounts.models import Prefectures2
from django.contrib.auth.models import User

register = template.Library()

@register.simple_tag
def data_verbose(bound_field):
    result = Prefectures2.objects.get(pk=bound_field)
    return result