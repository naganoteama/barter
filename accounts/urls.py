from django.urls import path, include
from . import views

app_name = 'account'
urlpatterns = [

    path('login/', views.MyLoginView.as_view(), name="login"),

    path('loginjudge/', views.loginjudgeview, name="loginjudge"),
    path('agreement/', views.AgreementView.as_view(), name="agreement"),

    path('user_data_input/', views.UserDataInput.as_view(), name='user_data_input'),
    path('user_data_confirm/', views.UserDataConfirm.as_view(), name='user_data_confirm'),
    path('user_data_create/', views.UserDataCreate.as_view(), name='user_data_create'),

    path('cansel/', views.UserDeleteConfirmView.as_view(), name='cansel'),
    path('canselconp/', views.UserDeleteView.as_view(), name='canselconp'),
    ]

