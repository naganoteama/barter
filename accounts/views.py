from django.shortcuts import redirect, render

from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from . import forms
from django.urls import reverse_lazy

from django.views import generic

from django.views.decorators.http import require_POST
from .forms import UserCreateForm
from django.contrib.auth import get_user_model
from accounts.models import CustomUser

from django.http import HttpResponseRedirect
from django.urls import reverse
from barter_app.models import Goods,ChatRoom,Offer
from django.db.models import Q 


User = get_user_model()


#初回ログイン判定
def loginjudgeview(request):
    user = request.user
    log_f = user.login_flg
    if log_f == True:
        return redirect('account:user_data_input')
    else:
        return HttpResponseRedirect(reverse('barter_app:home', args=request))


#ログイン
class MyLoginView(LoginView):
    form_class = forms.LoginForm
    template_name = "account/login.html"


# 規約ページ
class AgreementView(generic.TemplateView):
    template_name = 'agreement.html'



#ユーザー情報入力
class UserDataInput(generic.FormView):
    model = CustomUser
    template_name = 'user_data_input.html'
    form_class = UserCreateForm


    def form_valid(self, form):
        return render(self.request, 'user_data_input.html', {'form': form})
    
    


#ユーザ情報の確認
class UserDataConfirm(generic.FormView):
    form_class = UserCreateForm

    def form_valid(self, form):
        return render(self.request, 'user_data_confirm.html', {'form': form})

    def form_invalid(self, form):
        return render(self.request, 'user_data_input.html', {'form': form})

#ユーザデータ登録
class UserDataCreate(generic.UpdateView):
    model = CustomUser
    form_class = UserCreateForm
    success_url = reverse_lazy('barter_app:home')

    def post(self, request):
        user = request.user
        form = UserCreateForm(request.POST or None, instance=request.user)
        form.save()
        log_f = user.login_flg
        if log_f == True:
            user.login_flg = False
            user.save()
        return redirect("barter_app:home")
        #return render(self.request, 'home.html')
    
    def form_invalid(self, form):
        return render(self.request, 'user_data_input.html', {'form': form})


#退会ページ
class UserDeleteConfirmView(generic.TemplateView):
    template_name = 'cansel.html'


#退会処理
class UserDeleteView(LoginRequiredMixin, generic.View):
    def get(self, *args, **kwargs):
        user = User.objects.get(email=self.request.user.email)
        user.is_active = False
        user.save()
        goods_list = Goods.objects.filter(user=user)
        offer_list = Offer.objects.filter(user=user)
        for goods in goods_list:
            goods.trade = 3
            goods.save()
        for offer in offer_list:
            offer.trade = 5
            offer.save()
        room_list = ChatRoom.objects.filter(Q(user1=user.pk) | Q(user2=user.pk))
        for room in room_list:
            room.is_active = False
            room.save()
        #auth_logout(self.request)
        return render(self.request,'cansel_after.html')