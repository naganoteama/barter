from allauth.account.adapter import DefaultAccountAdapter


class AccountAdapter(DefaultAccountAdapter):
    def save_user(self, request, user, form, commit=True):
        user = super(AccountAdapter, self).save_user(request, user, form, commit=False)
        user.user_name = form.cleaned_data.get['user_name']
        user.last_name = form.cleaned_data.get['last_name']
        user.first_name = form.cleaned_data.get['first_name']
        user.last_kana = form.cleaned_data.get['last_kana']
        user.first_kana = form.cleaned_data.get['first_kana']
        user.address = form.cleaned_data.get['address']
        user.tel_num = form.cleaned_data.get['tel_num']
        user.save()