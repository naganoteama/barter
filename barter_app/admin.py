from django.contrib import admin
from .models import (Evaluation, Goods, Prefectures, Category1, Category2, Offer, GoodsComment,
 TradingHistory, TradingMessage,ChatRoom,ChatMessage, FavUser, TodosList, UserNotice)

admin.site.register(Goods)

admin.site.register(Prefectures)
admin.site.register(Category1)
admin.site.register(Category2)
admin.site.register(Offer)
admin.site.register(TradingHistory)
admin.site.register(TradingMessage)
admin.site.register(GoodsComment)

admin.site.register(Evaluation)

admin.site.register(FavUser)

admin.site.register(ChatRoom)
admin.site.register(ChatMessage)
admin.site.register(TodosList)
admin.site.register(UserNotice)