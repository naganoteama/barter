from django.apps import AppConfig


class BarterAppConfig(AppConfig):
    name = 'barter_app'
