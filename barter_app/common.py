from django.core.validators import ValidationError

# 禁止用語 
# ※各自で追加して下さい
NG_WORDS =[
    '死ね', '消えろ', 'バカ', '馬鹿', 'きもい', 'キモイ','ばか',
]

# NGワード
def NgWord(value):
    for ng in NG_WORDS:
        if value.find(ng) != -1:
            raise ValidationError('※不適切な用語が含まれています。')