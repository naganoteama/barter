from django import forms

from django.db.models import fields
from django.contrib.auth.forms import AuthenticationForm

from django.contrib.auth.models import User

from accounts.models import CustomUser

from .models import Goods, Offer, TradingMessage, Evaluation, ChatMessage, GoodsComment, Category2

from django.db.models import fields
from django.contrib.auth.forms import AuthenticationForm

from accounts.models import CustomUser
from django.core.mail import EmailMessage
from accounts.models import Prefectures2
from django import forms
from django.core.exceptions import ValidationError
from barter_app.common import NgWord# NGワード

# 商品の状態の選択肢
status_choice = (('新品未使用品','新品未使用品'), ('美品','美品'), ('普通','普通'), ('悪い','悪い'), ('非常に悪い','非常に悪い'),)

# ログイン
from django.contrib.auth import authenticate, get_user_model

# お問い合わせ
class InquiryForm(forms.Form):
    name = forms.CharField(label='お名前', max_length=30, validators=[NgWord])
    email = forms.EmailField(label='メールアドレス')
    message = forms.CharField(label='お問い合わせ内容', widget=forms.Textarea, validators=[NgWord])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['class'] = 'form-control col-9'
        self.fields['name'].widget.attrs['placeholder'] = 'お名前を入力してください。'

        self.fields['email'].widget.attrs['class'] = 'form-control col-9'
        self.fields['email'].widget.attrs['placeholder'] = 'メールアドレスをここに入力してください。'

        self.fields['message'].widget.attrs['class'] = 'form-control col-9'
        self.fields['message'].widget.attrs['placeholder'] = 'お問い合わせ内容をここに入力してください。'

    def send_email(self):
        name = self.cleaned_data['name']
        email = self.cleaned_data['email']
        message = self.cleaned_data['message']

        subject = 'お問い合わせ {}'.format(name)
        message = '□ 送信者名：{0}\n□ メールアドレス：{1}\n□ お問い合わせ内容：\n{2}'.format(name, email, message)
        from_email = 'admin@example.com'
        to_list = [
            'test@example.com'
        ]
        cc_list = [
            email
        ]

        message = EmailMessage(subject=subject, body=message, from_email=from_email, to=to_list, cc=cc_list)
        message.send()
    

# 商品出品フォーム
class GoodsCreateForm(forms.ModelForm):
    name = forms.CharField(label='商品名')
    category = forms.ModelChoiceField(queryset=Category2.objects.all())

    class Meta:
        model = Goods
        fields = ('name','content','category','status','photo1','photo2','photo3','photo4','photo5',)
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['status'].required= True

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'

# 商品交換申し込みフォーム
class GoodsOfferForm(forms.ModelForm):

    class Meta:
        model = Offer
        fields = ('name','content','category','status','photo1','photo2','photo3','photo4','photo5',)
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].required= True
        self.fields['status'].required= True

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'

        self.fields['name'].widget.attrs['class'] = 'form-control show-count'


# 設定変更フォーム
class SettingsChangeAccountForm(forms.ModelForm):
    username = forms.CharField(label="ユーザー名")
    last_name = forms.CharField(label='姓', max_length=254)
    first_name = forms.CharField(label='名')
    birthday = forms.CharField(label='生年月日', max_length=10)
    email = forms.EmailField(label='メールアドレス', max_length=254, required=True)
    telephone = forms.CharField(label='電話番号', max_length=11)
    zip_code = forms.CharField(label='郵便番号', max_length=254)
    prefecture = forms.ModelChoiceField(queryset=Prefectures2.objects.all())
    address = forms.CharField(label='住所(市区町村・番地)', max_length=254)


    class Meta:
        model = CustomUser
        fields = ('usericon', 'username','last_name','first_name', 'birthday', 'email', 'telephone', 'zip_code', 'prefecture', 'address', 'address2', 'comment', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
        self.fields['zip_code'].widget.attrs['class'] = 'form-control col-10 p-postal-code'
        self.fields['zip_code'].widget.attrs['placeholder'] = '例：0101234'

        self.fields['prefecture'].widget.attrs['class'] = 'form-control col-10 p-region-id'
        self.fields['prefecture'].widget.attrs['placeholder'] = '例：都道府県'

        self.fields['address'].widget.attrs['class'] = 'form-control col-10 p-locality p-street-address'
        self.fields['address'].widget.attrs['placeholder'] = '例：見本市見本町1-23-4'

        self.fields['address2'].widget.attrs['class'] = 'form-control col-10 p-extended-address'
        self.fields['address2'].widget.attrs['placeholder'] = '例：〇〇ビル301'


# アイコンのみ変更フォーム
class SettingsChangeAccountIconForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ('usericon',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'



# 取引メッセージフォーム
class TradingMessageForm(forms.ModelForm):
    class Meta:
        model = TradingMessage
        fields = ('message',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'



# 受取評価
class EvaluationForm(forms.ModelForm):

    class Meta:
        model = Evaluation
        fields = ('review',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.fields['review'].widget.attrs['class'] = 'form-control'
        self.fields['review'].widget.attrs['placeholder'] = '評価のコメントを入力してください。'


# チャットメッセージ送信
class ChatMessageForm(forms.ModelForm):
    class Meta:
        model = ChatMessage
        fields = ('message',)
        widgets = {
            'message': forms.Textarea(attrs={'rows':7}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'


# 商品コメント欄
class GoodsCommentForm(forms.ModelForm):
    class Meta:
        model = GoodsComment
        fields = ('comment',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'


# マイページ　パスワード変更
User = get_user_model()

class ChangePasswordForm(forms.Form):
    """パスワード変更"""
    current_password = forms.CharField(
        required=True,
        max_length=255,
        min_length=6,
        widget=forms.PasswordInput(),
    )
    new_password = forms.CharField(
        required=True,
        max_length=255,
        min_length=6,
        widget=forms.PasswordInput(),
    )
    confirm_new_password = forms.CharField(
        required=True,
        max_length=255,
        min_length=6,
        widget=forms.PasswordInput(),
    )
    def __init__(self, user_id, *args, **kwargs):
        self.user_id = user_id
        super().__init__(*args, **kwargs)

        self.fields['current_password'].widget.attrs['class'] = 'form-control col-9'
        self.fields['current_password'].widget.attrs['placeholder'] = '現在のパスワードを入力してください。'

        self.fields['new_password'].widget.attrs['class'] = 'form-control col-9'
        self.fields['new_password'].widget.attrs['placeholder'] = '新しいパスワードを入力してください。'

        self.fields['confirm_new_password'].widget.attrs['class'] = 'form-control col-9'
        self.fields['confirm_new_password'].widget.attrs['placeholder'] = '新しいパスワードを再入力してください。'


    def clean_current_password(self):
        current_password = self.cleaned_data['current_password']
        user = User.objects.get(id=self.user_id)
        if user.username and current_password:
            auth_result = authenticate(
                username = user.username,
                password=current_password,
            )
            if not auth_result:
                raise ValidationError('現在のパスワードが間違っています。')
        return current_password

    def clean_new_password(self):
        new_password = self.cleaned_data['new_password']
        return new_password

    def clean_confirm_new_password(self):
        confirm_new_password = self.cleaned_data['confirm_new_password']
        return confirm_new_password

    def clean(self):
        cleaned_data = super().clean()
        new_password = self.cleaned_data.get('new_password')
        confirm_new_password = self.cleaned_data.get('confirm_new_password')
        if new_password != confirm_new_password:
            self.add_error(
                field='confirm_new_password',
                error=ValidationError('パスワードが一致しません。'))
        return cleaned_data

class PrefectureSelectForm(forms.ModelForm):
    name = forms.ModelChoiceField(queryset=Prefectures2.objects.all(),required=False)
    
    class Meta:
        model = Prefectures2
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'
