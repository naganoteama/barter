import csv
import datetime
import os

from django.conf import settings
from django.core.management.base import BaseCommand

from ...models import Goods

class Command(BaseCommand):
    help = "Backup Goods data"

    def handle(self, *args, **options):
        date = datetime.datetime.now().strftime("%Y%m%d%H:%M")

        file_path = settings.BACKUP_PATH + 'goods_' + date + '.csv'

        os.makedirs(settings.BACKUP_PATH, exist_ok=True)

        with open(file_path, 'w') as file:
            writer = csv.writer(file)

            header = [field.name for field in Goods._meta.fields]
            writer.writerow(header)

            goods_list = Goods.objects.all()

            for goods in goods_list:
                writer.writerow([str(goods.id),
                                str(goods.name),
                                str(goods.status),
                                str(goods.category),
                                str(goods.user),
                                str(goods.prefecture),
                                str(goods.content),
                                str(goods.photo1),
                                str(goods.photo2),
                                str(goods.photo3),
                                str(goods.photo4),
                                str(goods.photo5),
                                str(goods.created_at),
                                str(goods.confirm),
                                str(goods.trade)])
        files = os.listdir(settings.BACKUP_PATH)
        if len(files) >= settings.NUM_SAVED_BACKUP:
            files.sort()
            os.remove(settings.BACKUP_PATH + files[0])
        print('終了')
        print(file_path)
