from django.shortcuts import redirect, render
from urllib import response


class FirstLogin(object):
    def __init__(self, get_response) -> None:
        self.get_response = get_response
    
    def __call__(self, request):
        if request.user.pk:
            if request.user.login_flg:
                if request.path != '/accounts/user_data_input/' and request.path != '/accounts/user_data_confirm/' and request.path != '/accounts/user_data_create/':
                    return redirect('account:user_data_input')
            #if request.user.login_flg and request.method=="GET":
        response = self.get_response(request)
        return response
    
class BackLog(object):
    def __init__(self, get_response) -> None :
        self.get_response = get_response

    def __call__(self, request):

        response = self.get_response(request)
        return response