from django.db import models
from accounts.models import CustomUser
from barter_app.common import NgWord# NGワード
from django.contrib.auth import get_user_model

# 商品の状態の選択肢
status_choice = (('新品未使用品','新品未使用品'), ('美品','美品'), ('普通','普通'), ('悪い','悪い'), ('非常に悪い','非常に悪い'),)


class Prefectures(models.Model):
    """都道府県モデル"""
    
    code = models.IntegerField(verbose_name='都道府県コード', primary_key=True)
    name = models.CharField(verbose_name='都道府県名', max_length=30)

    class Meta:
        verbose_name_plural = 'Prefectures'
    
    def __str__(self):
        return self.name

class Category1(models.Model):
    """大分類カテゴリーモデル"""

    cate1 = models.CharField(verbose_name='カテゴリー1', max_length=30)

    class Meta:
        verbose_name_plural = 'Category1'
    
    def __str__(self):
        return self.cate1

class Category2(models.Model):
    """詳細カテゴリーモデル"""
    
    cate1 = models.ForeignKey(Category1, verbose_name='カテゴリー1', on_delete=models.PROTECT, blank=True, null=True)
    cate2 = models.CharField(verbose_name='カテゴリー2', max_length=30)

    class Meta:
        verbose_name_plural = 'Category2'
    
    def __str__(self):
        return str(self.cate1) + ' - ' + str(self.cate2)

class Goods(models.Model):
    """出品商品モデル"""

    name = models.CharField(verbose_name='商品名', validators=[NgWord], max_length=30)
    status = models.CharField(verbose_name='商品状態', default='', max_length=10, blank=True, null=True, choices=status_choice)
    category = models.ForeignKey(Category2, verbose_name='カテゴリー', on_delete=models.PROTECT, blank=True, null=True)
    user = models.ForeignKey(CustomUser, verbose_name='出品者', on_delete=models.PROTECT)
    prefecture = models.ForeignKey(Prefectures, verbose_name='都道府県', on_delete=models.PROTECT, blank=True, null=True)
    content = models.TextField(verbose_name='商品説明', validators=[NgWord], max_length=200, default='', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='写真1', blank=True, null=True)
    photo2 = models.ImageField(verbose_name='写真2', blank=True, null=True)
    photo3 = models.ImageField(verbose_name='写真3', blank=True, null=True)
    photo4 = models.ImageField(verbose_name='写真4', blank=True, null=True)
    photo5 = models.ImageField(verbose_name='写真5', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='出品日時', auto_now_add=True)
    confirm = models.BooleanField(verbose_name='登録完了済み', default=True)
    trade = models.IntegerField(verbose_name='1:出品中2:取引成立3:出品取下げ', default=1)

    class Meta:
        verbose_name_plural = 'Goods'
    
    def __str__(self):
        return self.name


class Offer(models.Model):
    """交換申し込みモデル"""

    user = models.ForeignKey(CustomUser, verbose_name='交換申し込み者', on_delete=models.PROTECT)
    goods = models.ForeignKey(Goods, verbose_name='相手の商品', on_delete=models.PROTECT, blank=True, null=True)
    name = models.CharField(verbose_name='自分の商品名', validators=[NgWord], max_length=30)
    category = models.ForeignKey(Category2, verbose_name='カテゴリー', on_delete=models.PROTECT, blank=True, null=True)
    status = models.CharField(verbose_name='商品状態', default='', max_length=10, blank=True, null=True, choices=status_choice)
    prefecture = models.ForeignKey(Prefectures, verbose_name='都道府県', on_delete=models.PROTECT, blank=True, null=True)
    content = models.TextField(verbose_name='商品説明', validators=[NgWord], max_length=200, default='', blank=True, null=True)
    photo1 = models.ImageField(verbose_name='写真1', blank=True, null=True)
    photo2 = models.ImageField(verbose_name='写真2', blank=True, null=True)
    photo3 = models.ImageField(verbose_name='写真3', blank=True, null=True)
    photo4 = models.ImageField(verbose_name='写真4', blank=True, null=True)
    photo5 = models.ImageField(verbose_name='写真5', blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='申し込み日時', auto_now_add=True)
    confirm = models.BooleanField(verbose_name='登録完了済み', default=True)
    trade = models.IntegerField(verbose_name='1:申請中2:取引成立3:拒否4:出品終了5:申請取下げ', default=1)
    goods_registed_id = models.IntegerField(verbose_name='登録済み商品の場合ID,新規申し込みの場合:-1', blank=True, null=True, default=-1)

    class Meta:
        verbose_name_plural = 'Offer'
    
    def __str__(self):
        return self.name

class TradingHistory(models.Model):
    """取引履歴モデル"""

    goods = models.ForeignKey(Goods, verbose_name='出品者の商品', on_delete=models.PROTECT, blank=True, null=True)
    offer = models.ForeignKey(Offer, verbose_name='申込者の商品', on_delete=models.PROTECT, blank=True, null=True)
    completion_at = models.DateTimeField(verbose_name='取引成立日時', auto_now_add=True)
    sending1 = models.IntegerField(verbose_name='出品商品 1:未発送2:発送済み3:受け取り完了4:評価済み', default=1)
    sending2 = models.IntegerField(verbose_name='申込商品 1:未発送2:発送済み3:受け取り完了4:評価済み', default=1)
    status = models.IntegerField(verbose_name='1:取引中2:取引完了', default=1)
    g_eva = models.BooleanField(verbose_name='出品者評価済み', default=False)
    o_eva = models.BooleanField(verbose_name='申込者評価済み', default=False)
    
    class Meta:
        verbose_name_plural = 'TradingHistory'
    
    def __str__(self):
        return str(self.goods) + ' と ' + str(self.offer) + ' の交換'

class TradingMessage(models.Model):
    """取引メッセージ"""

    trading = models.ForeignKey(TradingHistory, verbose_name='取引履歴', on_delete=models.PROTECT, blank=True, null=True)
    sender = models.IntegerField(verbose_name='送信者ID', blank=True, null=True)
    receiver = models.IntegerField(verbose_name='受信者ID', blank=True, null=True)
    subject = models.CharField(verbose_name='件名', default='', max_length=30, blank=True, null=True)
    message = models.TextField(verbose_name='メッセージ', validators=[NgWord], default='', max_length=200, blank=True, null=True)
    completion_at = models.DateTimeField(verbose_name='送信日時', auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'TradingMessage'
    
    def __str__(self):
        return str(self.sender) + 'と' + str(self.receiver) + 'のメッセージ'
        
evaluation_choice = (('good', '良かった'), ('bad', '悪かった'),)
class Evaluation(models.Model):
    """受取評価画面"""

    user = models.ForeignKey(CustomUser, verbose_name='評価される側', on_delete=models.PROTECT)
    trading = models.ForeignKey(TradingHistory, verbose_name='取引履歴', on_delete=models.PROTECT, blank=True, null=True)
    eva_user = models.IntegerField(verbose_name='評価者ID', blank=True, null=True)
    eva_username = models.CharField(verbose_name='評価者名',default='', max_length=15, blank=True, null=True)
    eva_radio = models.CharField(verbose_name="受取評価",default='good', max_length=10)
    review = models.TextField(verbose_name='評価のコメント', validators=[NgWord], default='', max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='評価日時', auto_now_add=True, blank=True, null=True)
    confirm = models.BooleanField(verbose_name='評価確定済み',default=False)
    class Meta:
        verbose_name_plural = 'Evaluation'

    def __str__(self):
        return str(self.user) + ' ' + str(self.eva_radio)

class FavUser(models.Model):
    """お気に入り"""

    user = models.ForeignKey(CustomUser, verbose_name='お気に入り(人)', on_delete=models.CASCADE, related_name='send_user')
    item = models.ForeignKey(Goods, verbose_name='お気に入り(もの)', on_delete=models.CASCADE, related_name='receive_item')

    class Meta:
        verbose_name_plural = 'Favorites'

    def __str__(self):
        return str(self.user)


class ChatRoom(models.Model):
    """チャットルーム"""

    user1 = models.IntegerField(verbose_name='ユーザ1', blank=True, null=True)
    user2 = models.IntegerField(verbose_name='ユーザ2', blank=True, null=True)
    last_at = models.DateTimeField(verbose_name='最終メッセージ日時', auto_now_add=True)
    is_active = models.BooleanField(verbose_name='アクティブか（退会したらFalse）',default=True)
    
    class Meta:
        verbose_name_plural = 'ChatRoom'
    
    def __str__(self):
        return str(self.user1) + ' ' + str(self.user2) + str(self.last_at)

class ChatMessage(models.Model):
    """チャットメッセージ"""

    room = models.ForeignKey(ChatRoom, verbose_name='チャットルーム', on_delete=models.PROTECT, blank=True, null=True)
    sender = models.IntegerField(verbose_name='送信者ID', blank=True, null=True)
    message = models.TextField(verbose_name='メッセージ', default='', max_length=200,validators=[NgWord])
    created_at = models.DateTimeField(verbose_name='送信日時', auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'ChatMessage'
    
    def __str__(self):
        return self.message + str(self.created_at)

class GoodsComment(models.Model):
    """商品コメント欄"""

    goods = models.ForeignKey(Goods, verbose_name='商品', on_delete=models.PROTECT, blank=True, null=True)
    user = models.ForeignKey(CustomUser, verbose_name='コメント者', on_delete=models.PROTECT)
    comment = models.TextField(verbose_name='コメント', default='', max_length=100,validators=[NgWord])
    created_at = models.DateTimeField(verbose_name='送信日時', auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'GoodsComment'
    
    def __str__(self):
        return self.comment + str(self.created_at)

class TodosList(models.Model):
    """ Todoリスト """

    goods = models.ForeignKey(Goods, verbose_name='Goodsのid', on_delete=models.PROTECT, null=True, blank=True)
    offer = models.ForeignKey(Offer, verbose_name='OfferModelのid',on_delete=models.PROTECT, null=True, blank=True)
    trade = models.ForeignKey(TradingHistory, verbose_name='TradingHistoryのid',on_delete=models.PROTECT, null=True, blank=True)
    user = models.ForeignKey(CustomUser, verbose_name='出品者,(チャットだと受信者)', on_delete=models.PROTECT)
    photo1 = models.ImageField(verbose_name='出品者の写真1', blank=True, null=True)
    todo_flg = models.IntegerField(verbose_name='出品者 1:拒否2:未発送3:発送済4:受取完了5:取引終了6:削除', null=True, blank=True)
    offer_flg = models.IntegerField(verbose_name='申請者 0:申請中1:申請取下げ2:未発送3:発送済4:受取完了5:取引終了6:削除', default=0, null=True, blank=True)
    created_at = models.DateTimeField(verbose_name='日時(やることリスト登録日時)', auto_now=True)
    dis_flg1 = models.IntegerField(verbose_name='出品者1:表示2:非表示', null=True, blank=True)
    dis_flg2 = models.IntegerField(verbose_name='申請者1:表示2:非表示', null=True, blank=True)
    todoslist_flg = models.IntegerField(verbose_name='チャットの通知か否か 1:取引 2:チャット',default=1)
    chat_dis_flg=models.IntegerField(verbose_name='チャット通知1:表示2:非表示',null=True, blank=True)
    chat_gamen = models.IntegerField(verbose_name='どのチャット通知 1:ChatMessage2:GoodsComment3:TradingMessage',null=True,blank=True)
    user2 = models.IntegerField(verbose_name='チャット送信者', blank=True, null=True)

    class Meta:
        verbose_name_plural = 'TodosList'
    
    def __str__(self):
        return str(self.id) + str(self.user) + str(self.todoslist_flg) + str(self.offer)

class UserNotice(models.Model):
    """ユーザー新着お知らせ"""

    user = models.ForeignKey(CustomUser, verbose_name='コメント者', on_delete=models.PROTECT)
    category = models.IntegerField(verbose_name='分類 0:未設定1:申し込み2:交換成立3:チャット4:取引連絡5:評価された6:交換不成立', default=0)
    comment = models.TextField(verbose_name='コメント', default='', max_length=100)
    created_at = models.DateTimeField(verbose_name='お知らせ日時', auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'UserNotice'
    
    def __str__(self):
        return str(self.user) + str(self.created_at)
