from django import template
from barter_app.models import TodosList
from operator import attrgetter
from itertools import chain
from django.contrib.auth.models import User
from urllib import parse
from django.shortcuts import resolve_url
from accounts.models import CustomUser

register = template.Library()

@register.simple_tag # todoリストの件数を出す
def get_todos(user1):

    todos_user = TodosList.objects.filter(user=user1, dis_flg1=1, todoslist_flg=1) # 出品者が自分のtodoリスト
    todos_offer = TodosList.objects.filter(trade__offer__user=user1, dis_flg2=1, todoslist_flg=1) # 申請者が自分のtodoリスト
    todos_offer2 = TodosList.objects.filter(offer__user=user1,todo_flg=1, dis_flg2=1, todoslist_flg=1) # 申請した商品が拒否されたtodoリスト
    todos_offer3 = TodosList.objects.filter(offer__user=user1,todo_flg=6, dis_flg2=1, todoslist_flg=1) # 申請した商品が削除されたtodoリスト
    todos_chat = TodosList.objects.filter(user=user1, todoslist_flg=2, chat_dis_flg=1) # チャットの相手が自分のtodoリスト

    todos_list = sorted(chain(todos_user, todos_offer, todos_offer2, todos_offer3, todos_chat), key=attrgetter('created_at')) # 上の2つリストに入れて日付順に並べた

    t = len(todos_list)
    return t

@register.simple_tag # チャットの相手の情報を出す
def get_user(user2, num):
    if num == 1:   # アイコン
        user2_icon = CustomUser.objects.get(pk=user2)
        if user2_icon.usericon:
            icon = user2_icon.usericon.url
        else:
            icon = 0
        return icon
    elif num == 2: # 名前
        user2_name = CustomUser.objects.get(pk=user2)
        name = user2_name.username
        return name

@register.simple_tag # 個人チャットの未読を出す
def get_chat(user1, u2_pk):

    if TodosList.objects.filter(user=user1, user2=u2_pk, todoslist_flg=2, chat_dis_flg=1, chat_gamen=1): # チャットの未読があれば
        chat = "未読あり"
    else:
        chat = 0
    return chat

@register.simple_tag # 取引チャットの未読を出す
def get_chat2(user1, trade):

    if TodosList.objects.filter(trade=trade, user=user1, todoslist_flg=2, chat_dis_flg=1, chat_gamen=3): # チャットの未読があれば
        chat = "未読あり"
    else:
        chat = 0
    return chat

@register.simple_tag # 商品コメントの未読を出す
def get_chat3(user1, goods):

    if TodosList.objects.filter(goods=goods, user=user1, todoslist_flg=2, chat_dis_flg=1, chat_gamen=2): # チャットの未読があれば
        chat = "未読あり"
    else:
        chat = 0
    return chat
