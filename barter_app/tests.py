from django.test import TestCase
from django.shortcuts import redirect, render
from django.http import HttpResponse, request
from accounts . models import CustomUser, Prefectures2
from . models import Category1, Category2, Prefectures, Goods, Evaluation
from django.db.models import Q 
import ast, json

PREFECTURES_choice = (
    (1, '北海道'),    (2, '青森県'),    (3, '岩手県'),    (4, '宮城県'),
    (5, '秋田県'),    (6, '山形県'),    (7, '福島県'),    (8, '茨城県'),
    (9, '栃木県'),    (10, '群馬県'),    (11, '埼玉県'),    (12, '千葉県'),
    (13, '東京都'),    (14, '神奈川県'),    (15, '新潟県'),    (16, '富山県'),
    (17, '石川県'),    (18, '福井県'),    (19, '山梨県'),    (20, '長野県'),
    (21, '岐阜県'),    (22, '静岡県'),    (23, '愛知県'),    (24, '三重県'),
    (25, '滋賀県'),    (26, '京都府'),    (27, '大阪府'),    (28, '兵庫県'),
    (29, '奈良県'),    (30, '和歌山県'),    (31, '鳥取県'),    (32, '島根県'),
    (33, '岡山県'),    (34, '広島県'),    (35, '山口県'),    (36, '徳島県'),
    (37, '香川県'),    (38, '愛媛県'),    (39, '高知県'),    (40, '福岡県'),
    (41, '佐賀県'),    (42, '長崎県'),    (43, '熊本県'),    (44, '大分県'),
    (45, '宮崎県'),    (46, '鹿児島県'),    (47, '沖縄県'),    (48, 'その他'),
)

Category_choice1 =('おもちゃ・グッズ', 'CD', 'DVD')
Category_choice2 =(
    (1,'おもちゃ'), (1,'タレントグッズ'), (1,'コミック/アニメグッズ'), (1,'フィギュア'),
    (2,'洋楽'),(2,'アニメ'),(2,'クラシック'),(2,'K-POP/アジア'),(2,'キッズ/ファミリー'),(2,'その他'),
    (3,'外国映画'),(3,'日本映画'),(3,'アニメ'),(3,'TVドラマ'),(3,'ミュージック'),(3,'お笑い/バラエティ'),
    (3,'スポーツ/フィットネス'),(3,'キッズ/ファミリー'),(3,'その他')
)

# 都道府県とカテゴリをデータベースに登録する関数
def makeData(request):
    '''
    for i in range(10):
        eva = Evaluation()
        eva.user = request.user
        eva.eva_radio = 'good'
        eva.eva_user = 1
        eva.eva_username = 'テスト' + str(i)
        eva.review = '最高の取引相手でした。また機会があればよろしくお願いいたします。あああああああああああああああああああああああああああああああああああああああああああああああ'
        eva.confirm = True
        eva.save()
    for i in range(10):
        eva = Evaluation()
        eva.user = request.user
        eva.eva_radio = 'bad'
        eva.eva_user = 1
        eva.eva_username = 'テスト' + str(i)
        eva.review = '最低の取引相手でした。2度と取引したくありません。あああああああああああああああああああああああああああああああああああああああああああああああああああああああああ'
        eva.confirm = True
        eva.save()
    '''

    for i in range(48):
        data = Prefectures()
        data.code = i + 1
        data.name = PREFECTURES_choice[i][1]
        data.save()

    for i in range(48):
        data = Prefectures2()
        data.code = i + 1
        data.name = PREFECTURES_choice[i][1]
        data.save()
    
    for i in range(3):
        cate = Category1()
        cate.pk = i + 1
        cate.cate1 = Category_choice1[i]
        cate.save()

    for i in range(len(Category_choice2)):
        cate2 = Category2()
        cate2.pk = str(Category_choice2[i][0]) + str(i)
        cate1 = Category1.objects.get(pk=Category_choice2[i][0])
        cate2.cate1 = cate1
        cate2.cate2 = Category_choice2[i][1]
        cate2.save()

    return render(request,'goods_list.html',{})

# データベースの商品情報をテキストに保存
def goods_copy(request):
    goods_list = Goods.objects.filter(trade=1)
    all_dict = {}
    num = 0
    all_list = []
    all_str = ''
    for goods in goods_list:
        goods_dict = {}
        goods_dict['name'] = goods.name
        goods_dict['status'] = goods.status
        goods_dict['content'] = goods.content
        if goods.category:
            goods_dict['category'] = goods.category.pk
        if goods.photo1:
            goods_dict['photo1'] = goods.photo1.url
        if goods.photo2:
            goods_dict['photo2'] = goods.photo2.url
        if goods.photo3:
            goods_dict['photo3'] = goods.photo3.url
        if goods.photo4:
            goods_dict['photo4'] = goods.photo4.url
        if goods.photo5:
            goods_dict['photo5'] = goods.photo5.url
        num += 1
        all_dict[num] = goods_dict
        all_list.append(goods_dict)
        all_str += str(goods_dict) + '\n'
    f = open('data.txt', 'w')
    f.write(all_str)
    f.close()
    return render(request,'goods_list.html',{})

# テキストの商品情報をデータベースに登録
def goods_read(request):
    f = open('data.txt', 'r')
    data_list = f.readlines()
    f.close()
    data_dic = {}

    for data in data_list:
        d = ast.literal_eval(str(data))
        goods = Goods()
        goods.user = request.user
        goods.name = d['name']
        goods.status = d['status']
        goods.content = d['content']
        goods.name = d['name']
        if d.get('category'):
            goods.category = Category2.objects.get(pk=d['category'])
        if d.get('photo1'):
            goods.photo1 = d['photo1'][6:]
        if d.get('photo2'):
            goods.photo1 = d['photo2'][6:]
        if d.get('photo3'):
            goods.photo1 = d['photo3'][6:]
        if d.get('photo4'):
            goods.photo1 = d['photo4'][6:]
        if d.get('photo5'):
            goods.photo1 = d['photo5'][6:]
        goods.save()

    #data_list = data_list.split('/n')

    #data_dic = ast.literal_eval(data_list)
    #for data in data_list:
    #    for k,v in dict(data).items:
    #        a = 0
    
    return render(request,'goods_list.html',{'d':d,'data':data_list})
