from django.urls import path
from . import views
from . import tests

app_name = 'barter_app'


urlpatterns = [

    path('',views.IndexView.as_view(), name="index"),
    
    path('goods-list/', views.GoodsListView.as_view(), name="goods_list"),
    path('goods-create/', views.GoodsCreateView.as_view(), name="goods_create"),

    path('',views.IndexView.as_view(), name="index"),
    path('q_a/', views.Q_aView.as_view(), name="q_a"),

    path('inquiry/',views.InquiryForm.as_view(), name="inquiry"),

    path('home/', views.HomeView.as_view(), name="home"),

    path('mypage/', views.MypageView.as_view(), name="mypage"),
    path('settings_change_account/', views.SettingsChangeAccount, name="settings_change_account"),

    path('inquiry/',views.InquiryForm.as_view(), name="inquiry"),

    path('settings_change_account_icon_only/', views.SettingsChangeAccountIcon, name="settings_change_account_icon_only"),
    path('change_password/', views.change_password, name="change_password"),
    path('change_success/', views.change_successView.as_view(), name="change_success"),

    path('goods_list/', views.GoodsListView.as_view(), name="goods_list"),
    path('goods_create/', views.GoodsCreateView.as_view(), name="goods_create"),
    path('goods_detail/<int:pk>/', views.goods_detail, name="goods_detail"),
    path('goods_delete/', views.goods_delete,name="goods_delete"),
    path('offer_create/', views.GoodsOffer, name="offer_create"),
    path('offer_select/', views.OfferSelect, name="offer_select"),
    path('my_offer/', views.my_offer, name="my_offer"),
    path('offer_detail/', views.offer_detail, name="offer_detail"),
    path('trading_history/', views.trading_history,name="trading_history"),
    path('trading_history_end/', views.trading_history_end,name="trading_history_end"),
    path('trading_detail/', views.trading_detail,name="trading_detail"),
    path('trading_contact/', views.trading_contact,name="trading_contact"),
    path('chat_message/', views.chat_message,name="chat_message"),
    path('chat_room_list/', views.chat_room_list,name="chat_room_list"),
    path('user_notice/', views.user_notice,name="user_notice"),
    path('user_list/', views.user_list,name="user_list"),
    path('transaction_example/', views.transaction_example,name="transaction_example"),
    path('transaction_detail/<int:pk>/', views.transaction_detail, name="transaction_detail"),

    path('make-data/', tests.makeData,name="make_data"), # 都道府県、カテゴリデータ読み込み
    path('goods_copy/', tests.goods_copy,name="goods_copy"), # 商品データ保存
    path('goods_read/', tests.goods_read,name="goods_read"), # 商品データ読み込み

    path('goods-search/', views.goods_search,name="goods_search"),
    path('trade-search/', views.trade_search,name="trade_search"),

    path('prefecture_list/', views.prefectures_select,name="prefecture_list"),
    path('prefecture/<int:code>/', views.prefecture,name="prefecture"),

    path('ticket/', views.TicketView.as_view(), name="ticket"),
    path('pay-comp/', views.PayCompView.as_view(), name="pay_comp"),

    path('seller_information/', views.seller_info, name="seller_information"),
    path('like/', views.like, name="like"),
    path('like_list/', views.likelist, name="likelist"),
    path('user_evaluation/', views.user_evaluation, name="user_evaluation"),
    path('user_evaluation_list/<int:pk>/', views.user_evaluation_list, name="user_evaluation_list"),

    path('my-display', views.MydisplayView.as_view(), name="my_display"),
    path('todos/', views.TodosView, name="todos"),
    path('tododel/<int:pk>/', views.tododelview, name="todo_del"),
    path('address_print/', views.address_print, name="address_print"),
    path('seller_goods_detail/<int:pk>/', views.seller_goods_detail, name="seller_goods_detail"),

]
