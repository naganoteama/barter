#from asyncio.windows_events import NULL
from re import template
from django.contrib.messages.api import success
from django.db import models
from django.shortcuts import redirect, render
from django.views import generic

import datetime
from .forms import (ChangePasswordForm,ChatMessageForm, InquiryForm, GoodsCreateForm, GoodsOfferForm, GoodsCommentForm,
SettingsChangeAccountForm, SettingsChangeAccountIconForm, TradingMessageForm, EvaluationForm, PrefectureSelectForm)

from . models import (Goods, Category1, Category2, Offer, Prefectures, FavUser,
    TradingHistory, TradingMessage, Evaluation, ChatRoom, ChatMessage,GoodsComment, UserNotice)

from django.contrib.auth import authenticate, get_user_model
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.views.generic import CreateView, ListView
from django.db.models import Q 
from django.views import View
import payjp
from operator import attrgetter
from itertools import chain
from . models import TodosList

from accounts.models import CustomUser,Prefectures2
import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404

import json
from django.http import JsonResponse 

logger = logging.getLogger(__name__)


# トップページ
class IndexView(generic.TemplateView):
    template_name = 'index.html'


# マイページ
class MypageView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'mypage.html'

    def get(self, request):
        user = self.request.user
        offer_list_now = Offer.objects.filter(Q(user=request.user, confirm=True, trade=1) | Q(goods__user=request.user, confirm=True, trade=1)).order_by('-created_at')# 申請中
        offer_list_failure = Offer.objects.filter(Q(user=request.user, confirm=True, trade=3) | Q(goods__user=request.user, confirm=True, trade=3)).order_by('-created_at')# 申請不成立
        offer_list_already = Offer.objects.filter(Q(user=request.user, confirm=True, trade=2) | Q(goods__user=request.user, confirm=True, trade=2)).order_by('-created_at')# 取引済み
        context = {
            'offer_list_now':offer_list_now,
            'offer_list_failure':offer_list_failure,
            'offer_list_already':offer_list_already,
        }
        return render(request, "mypage.html", context)

# 設定ページ
@login_required
def SettingsChangeAccount(request):
    user = CustomUser.objects.get(pk=request.user.pk)
    initial_dict = dict(
                        usericon=user.usericon, 
                        username=user.username, 
                        last_name=request.user.last_name,
                        first_name=request.user.first_name,
                        birthday=user.birthday, 
                        email=user.email,
                        telephone=user.telephone, 
                        zip_code=user.zip_code,
                        prefecture=user.prefecture, 
                        address=user.address,  
                        address2=user.address2,
                        comment=user.comment
                        )# 現在のデータを表示・初期値 initail_dict
    form =  SettingsChangeAccountForm(request.POST or None, request.FILES or None, initial=initial_dict)
    context = {
        'user': user,
        'form': form,
    }
    if request.method == 'POST':
        if form.is_valid():
            if request.POST.get('usericon-clear'):# クリアをチェックした場合
                user.usericon = ''
            elif request.FILES:
                user.usericon = request.FILES['usericon']
            
            user.username = request.POST.get('username')
            user.last_name = request.POST.get('last_name')
            user.first_name = request.POST.get('first_name')
            user.birthday = request.POST.get('birthday')
            user.email = request.POST.get('email')
            user.telephone = request.POST.get('telephone')
            user.zip_code = request.POST.get('zip_code')
            user.prefecture = Prefectures2.objects.get(code=request.POST.get('prefecture'))# 都道府県コードから名を変換
            user.address = request.POST.get('address')
            user.address2 = request.POST.get('address2')
            user.comment = request.POST.get('comment')
            user.save()
        else:
            return render(request, 'settings_change_account.html', context, dict(form=form))

        return redirect('barter_app:mypage')# ページに飛ぶ処理 redirect

    return render(request, 'settings_change_account.html', context, dict(form=form))

# 設定ページIcon
def SettingsChangeAccountIcon(request):
    user = CustomUser.objects.get(pk=request.user.pk)
    initial_dict = dict(usericon=user.usericon)# 現在のデータを表示・初期値 initail_dict
    form =  SettingsChangeAccountIconForm(request.POST or None, request.FILES or None, initial=initial_dict)
    context = {
        'user': user,
        'form': form,
    }
    if request.method == 'POST':
        if form.is_valid():
            if request.POST.get('usericon-clear'):# クリアをチェックした場合
                user.usericon = ''
            elif request.FILES:
                user.usericon = request.FILES['usericon']
            user.save()
        else:
            return render(request, 'settings_change_account_icon_only.html', context, dict(form=form))

        return redirect('barter_app:mypage')# ページに飛ぶ処理 redirect

    return render(request, 'settings_change_account_icon_only.html', context, dict(form=form))
    

# お問い合わせページ
class InquiryForm(generic.FormView):
    template_name = 'inquiry.html'
    form_class = InquiryForm
    success_url = reverse_lazy('barter_app:inquiry')

    def form_valid(self, form):
        form.send_email()
        messages.success(self.request, 'お問い合わせ内容を送信しました。')
        logger.info('Inquiry sent by {}'.format(form.cleaned_data['name']))
        return super().form_valid(form)


#Q&Aページ
class Q_aView(generic.TemplateView):
    template_name = 'q_a.html'


# 商品一覧ページ(ログイン前)
class GoodsListView(generic.TemplateView):
    template_name = 'goods_list.html'
    model = Goods

    def get(self, request):
        if self.request.user.pk:
            goods = Goods.objects.filter(confirm=True,trade=1).exclude(user=self.request.user).order_by('-created_at')
        else:
            goods = Goods.objects.filter(confirm=True,trade=1).order_by('-created_at')
        prefecture_form = PrefectureSelectForm(self.request.POST or None)
        paginator = Paginator(goods, 48) # ページネーション
        page = self.request.GET.get('page')
        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        new_time = datetime.datetime.now() - datetime.timedelta(days=1) # 1日前の日時を取得
        context = {
            'new_time':new_time.strftime('%Y-%m-%d-%H'),
            'prefecture_form':prefecture_form,
            #'goods_list': goods,
            'goods_list': page_obj.object_list,
            'page_obj': page_obj,
        }
        return render(self.request, "goods_list.html", context)


# 商品登録ページ
class GoodsCreateView(LoginRequiredMixin, generic.CreateView):
    model = Goods
    template_name = 'goods_create.html'
    form_class = GoodsCreateForm
    success_url = reverse_lazy('barter_app:goods_list')

    def form_valid(self, form):
        context = {
            'form': form,
        }
        # 確認画面
        if self.request.POST.get('next', '') == 'confirm':
            Goods.objects.filter(user=self.request.user,confirm=False).delete()
            if form.is_valid():
                goods = Goods()
                goods.user = self.request.user
                goods.name = self.request.POST.get('name')
                goods.content = self.request.POST.get('content')
                goods.category =  Category2.objects.get(pk=self.request.POST.get('category'))
                goods.status = self.request.POST.get('status')

                photo_list = ['photo1','photo2','photo3','photo4','photo5']
                if self.request.FILES:
                    for p in photo_list:
                        if self.request.POST.get(p) == None:
                            if goods.photo1 == None:
                                goods.photo1 = self.request.FILES[p]
                            elif goods.photo2 == None:
                                goods.photo2 = self.request.FILES[p]
                            elif goods.photo3 == None:
                                goods.photo3 = self.request.FILES[p]
                            elif goods.photo4 == None:
                                goods.photo4 = self.request.FILES[p]
                            elif goods.photo5 == None:
                                goods.photo5 = self.request.FILES[p]
                goods.confirm = False
                goods.save()
                context = {
                    'form': form,
                    'goods':goods,
                }
            return render(self.request, 'goods_confirm.html',context)
        if self.request.POST.get('next', '') == 'back':
            goods = Goods.objects.get(id=self.request.POST.get('goods_id'))
            goods.delete()
            return render(self.request, 'goods_create.html',context)
        if self.request.POST.get('next', '') == 'create':
            goods = Goods.objects.get(id=self.request.POST.get('goods_id'))
            goods.confirm = True
            goods.save()
            messages.success(self.request, "商品を登録しました。")
            return redirect('barter_app:my_display')
    
#    def form_invalid(self, form):
#        messages.error(self.request, "商品の登録に失敗しました。")
#        return super().form_invalid(form)

# 商品詳細
def goods_detail(request, pk):
    if request.POST.get('nex', '') == 'goods_detail': # 申請詳細・取引詳細で新規の商品名リンクを押した場合
        if request.POST.get('link', ''):
            request.session['link']=request.POST.get('link')

        if 'link' in request.session:
            link = request.session['link']
            if link == 't_detail':
                trade = TradingHistory.objects.get(pk=request.POST["trade_pk"])
                offer = trade.offer
                context = {
                    'trade':trade,
                    'offer':offer,
                    'link': link,
                    'link2':request.session['link2'],
                }

            if link == 'o_detail':
                offer = Offer.objects.get(pk=request.POST["offer_pk"]) 
                context = {
                    'offer':offer,
                    'link': link,
                    'link2': request.session['link2'],
                }

    else: # 上記以外の場合
        goods = Goods.objects.get(pk=pk)
        comments = GoodsComment.objects.filter(goods=goods)
        form = GoodsCommentForm(request.POST or None)
        if request.user.pk: # ログイン中
            send_user = request.user
            exist_fav_user = FavUser.objects.filter(user=send_user, item_id=goods)
            is_exist_fav = exist_fav_user.exists()
        else: # ログインしていない場合
            is_exist_fav = False
        if request.POST.get('link', ''):
            request.session['link']=request.POST.get('link')
        context = {
            'object':goods,
            'form': form,
            'comments':comments,
            'is_exist_fav':is_exist_fav,
            'link' : request.session['link'],
        }
        if 'link' in request.session:
            link = request.session['link']
            if link == 't_detail':
                trade = TradingHistory.objects.get(pk=request.POST["trade_pk"])
                offer = trade.offer
                link2 = request.session['link2']
                context['trade'] = trade
                context['offer'] = offer
                context['link2'] = link2
            if link == 'o_detail':
                offer = Offer.objects.get(pk=request.POST["offer_pk"]) 
                link2 = request.session['link2']
                context['offer'] = offer
                context['link2'] = link2
 
        if request.POST.get('click', '') == 'on': # 商品コメントの通知を押した場合
            todos = TodosList.objects.get(pk=request.POST.get('todo_pk'))
            todos.chat_dis_flg = 2
            todos.save()
        if request.user.pk: # 出品した商品から来てやることリストに通知があった場合
            if TodosList.objects.filter(goods=goods.pk, user=request.user, chat_dis_flg=1, chat_gamen=2):
                todo = TodosList.objects.get(goods=goods.pk, user=request.user, chat_dis_flg=1, chat_gamen=2)
                todo.chat_dis_flg=2 # 非表示にする
                todo.save()
        context['come'] = 'newoffer' # 新規の商品と区別するため
        if request.POST: # コメント投稿
            #if not request.user.pk:
            #    return redirect('account:login')
            if form.is_valid():
                latest_come = GoodsComment.objects.filter(goods=goods,user=request.user).order_by('-created_at') # 同じコメントを連投した場合
                if latest_come:
                    if latest_come[0].comment == request.POST.get('comment'):
                        context['form'] = GoodsCommentForm()
                        return render(request, 'goods_detail.html', context)
                comment = form.save(commit=False)
                comment.goods = goods
                comment.user = request.user
                comment.save()
                user1 = int(request.user.pk)
                context['form'] = GoodsCommentForm()
                if goods.user != request.user:
                    if not TodosList.objects.filter(goods=goods,todoslist_flg=2,chat_dis_flg=1,chat_gamen=2): # todoリストに通知がなければ表示で登録
                        todos = TodosList.objects.create(goods=goods, user=goods.user, user2=user1,todoslist_flg=2,chat_dis_flg=1,chat_gamen=2)
                    if TodosList.objects.filter(goods=goods,todoslist_flg=2,chat_dis_flg=1,chat_gamen=2): # todoリストに通知があれば非表示で登録
                        todos = TodosList.objects.create(goods=goods, user=goods.user, user2=user1,todoslist_flg=2,chat_dis_flg=2,chat_gamen=2)
                return render(request, 'goods_detail.html', context)
            else:
                return render(request, 'goods_detail.html', context)

    return render(request, 'goods_detail.html', context)


#お気に入り 
@login_required
def like(request):
    if request.method =="POST":
        item = get_object_or_404(Goods,pk=request.POST["article_id"])
        user = request.user
        liked = False
        like = FavUser.objects.filter(item=item, user=user)
        if like.exists():
            like.delete()
        else:
            like.create(item=item, user=user)
            liked = True

        context={
            'article_id': item.id,
            'liked': liked,
        }

    print(request.is_ajax())
    return JsonResponse(context)


#お気に入り一覧
@login_required
def likelist(request):
    item = FavUser.objects.filter(user=request.user)
    context = {
        'favorite_list': item,
    }
    return render(request, 'like_list.html', context)

    def post(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


# 商品削除ページ
class GoodsDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Goods
    template_name = 'goods_delete.html'
    success_url =reverse_lazy('barter_app:goods_list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, "商品を削除しました。")
        return super().delete(request, *args, **kwargs)


#プロフィール
class SellerInformationView(generic.TemplateView):
    template_name = 'seller_information.html'


# 商品出品取り消しページ
@login_required
def goods_delete(request):
    if request.POST:
        goods = Goods.objects.get(pk=request.POST.get("goods_id"))
        context = {
            'goods':goods,
        }
            
        if request.POST.get('next', '') == 'delete':
            if TodosList.objects.filter(offer__goods__id=request.POST.get("goods_id")):  # もし申請した商品が削除されたら
                todos = TodosList.objects.filter(offer__goods__id=request.POST.get("goods_id"))
                for t in todos:
                    t.todo_flg = 6
                    t.offer_flg = 6
                    t.dis_flg1 = 2
                    t.save()
            if TodosList.objects.filter(offer__goods_registed_id=request.POST.get("goods_id")):  # もし申請している商品が削除されたら
                todos = TodosList.objects.filter(offer__goods_registed_id=request.POST.get("goods_id"))
                todos.offer_flg = 6
                for t in todos:
                    t.dis_flg1 = 2
                    t.dis_flg2 = 2
                    t.save()

            goods.trade = 3
            goods.save()
            offer_list = Offer.objects.filter(goods=goods, trade=1)
            for o in offer_list:
                if o.goods_registed_id == -1:
                    o_user = CustomUser.objects.get(pk=o.user.pk)
                    o_user.ticket += 1
                    o_user.save()
                else:
                    if Offer.objects.filter(goods_registed_id=o.goods_registed_id, trade=1).count() == 1:
                        o_user = CustomUser.objects.get(pk=o.user.pk)
                        o_user.ticket += 1
                        o_user.save()
                o.trade = 4
                o.save()
            offer_list = Offer.objects.filter(goods_registed_id=goods.pk, trade=1)
            if offer_list:
                user = CustomUser.objects.get(pk=request.user.pk)
                user.ticket += 1
                user.save()
            for o in offer_list:
                o.trade = 4
                o.save()
            messages.success(request, "商品の出品を取り消しました。")
            return redirect('barter_app:my_display')
        return render(request, 'goods_delete.html', context)
    return redirect('barter_app:my_display')

# トップページ(ログイン後ページ)
class HomeView(generic.TemplateView):
    template_name = 'home.html'
    model = Goods

    def get(self, request):
        if self.request.user.pk:
            goods = Goods.objects.filter(confirm=True,trade=1).exclude(user=self.request.user).order_by('-created_at')
        else:
            goods = Goods.objects.filter(confirm=True,trade=1).order_by('-created_at')
        prefecture_form = PrefectureSelectForm(self.request.POST or None)
        paginator = Paginator(goods, 48) # ページネーション
        page = self.request.GET.get('page')
        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        new_time = datetime.datetime.now() - datetime.timedelta(days=1) # 1日前の日時を取得
        context = {
            'new_time':new_time.strftime('%Y-%m-%d-%H'),
            'prefecture_form':prefecture_form,
            #'goods_list': goods,
            'goods_list': page_obj.object_list,
            'page_obj': page_obj,
        }
        return render(self.request, "home.html", context)

# チケット購入画面
class TicketView(LoginRequiredMixin, generic.TemplateView):
    model = CustomUser
    template_name = "ticket.html"
    """
    use PAY.JP API
    """
    def get(self, request):

        context = {
            # 公開鍵等を渡す        
            "public_key": "pk_test_8e5838abb26b43bee91d4a41",
        }

        return render(request, "ticket.html", context)

    def post(self, request, *args, **kwargs):
        user = self.request.user

        request.session['page']=self.request.POST.get('page') # どのページから飛んできたか
        se_page = request.session['page']

        context = {
                    # 公開鍵等を渡す        
                    "public_key": "pk_test_8e5838abb26b43bee91d4a41",
                    "user" : user,
                    "se_page" : se_page,
                }

        if se_page == 'of_new': # 新規申し込みなら
            goods = Goods.objects.get(pk=request.POST.get("goods_id")) # 出品者の商品
            offer = Offer.objects.get(id=request.POST.get("offer_id")) # 申請者の新規商品
            form = request.POST.get("form")
            context['goods'] = goods
            context['offer'] = offer
            context['form'] = form

        if se_page == 'of_select': # 登録済みの商品からなら
            goods = Goods.objects.get(pk=request.POST.get("goods_id")) # 出品者の商品
            my_goods = Goods.objects.get(pk=request.POST.get("my_goods_id")) # 申請者の商品
            context['goods'] = goods
            context['my_goods'] = my_goods            

        if se_page == 'tra_se': # 申請きた商品を承諾するところからなら
            goods = Goods.objects.get(pk=request.POST.get("goods_id")) # 出品者の商品
            offer = Offer.objects.get(pk=request.POST.get("offer_id")) # 申請者の新規商品
            context['goods'] = goods
            context['offer'] = offer

        return render(request, "ticket.html", context)

# 支払画面
class PayCompView(LoginRequiredMixin, generic.TemplateView, generic.UpdateView):
    template_name = 'pay_comp.html'
    model = CustomUser

    def get(self, request):

        return render(request, "pay_comp.html",)

    def post(self, request):
        user = self.request.user
        amount = request.POST.get("amount")
        payjp_token = request.POST.get("payjp-token")
        ticket = user.ticket
        
        new_ticket = 0 
        amount = int(amount)
        if amount == 300:
            ticket += 1
            new_ticket += 1
        elif amount == 500:
            ticket += 3
            new_ticket += 3
        elif amount == 1300:
            ticket += 5
            new_ticket += 5

        user.ticket = ticket
        user.save()    #DBにticket枚数更新

        # トークンから顧客情報を生成
        customer = payjp.Customer.create(email="example@pay.jp", card=payjp_token)
        # 支払いを行う
        charge = payjp.Charge.create(
            amount=amount,
            currency="jpy",
            customer=customer.id,
            description="Django example charge",
        )
        if 'page' in request.session:
            se_page = request.session['page']
        else:
            se_page = "pay"   

        context = {
            "amount": amount, 
            "ticket": new_ticket,
            "se_page":se_page,
            }

        if se_page == 'of_new': # 新規申し込みなら
            goods = Goods.objects.get(pk=request.POST.get("goods_id")) # 出品者の商品
            offer = Offer.objects.get(id=request.POST.get("offer_id")) # 申請者の新規商品
            context['goods'] = goods
            context['offer'] = offer
 
        if se_page == 'of_select': # 登録済みの商品からなら
            goods = Goods.objects.get(pk=request.POST.get("goods_id")) # 出品者の商品
            my_goods = Goods.objects.get(pk=request.POST.get("my_goods_id")) # 申請者の商品
            context['goods'] = goods
            context['my_goods'] = my_goods

        if se_page == 'tra_se': # 申請きた商品を承諾するところからなら
            offer = Offer.objects.get(pk=request.POST.get("offer_id")) # 申請者の新規商品
            goods = Goods.objects.get(pk=request.POST.get("goods_id")) # 出品者の商品
            context['offer'] = offer
            context['goods'] = goods          

        if 'page' in request.session:
            del request.session['page']

        return render(request, "pay_comp.html", context)

# 商品検索
def goods_search(request):
    if request.user.pk:
        goods_list = Goods.objects.filter(confirm=True,trade=1).exclude(user=request.user).order_by('-created_at')
    else:
        goods_list = Goods.objects.filter(confirm=True,trade=1).order_by('-created_at')
    user_pk = request.GET.get('user_pk')
    category1 = request.GET.get('category1')
    category2 = request.GET.get('category2')
    prefecture_code = request.GET.get('name')
    word = request.GET.get('word')
    words = ''
    if user_pk: # ユーザー別商品
        user = CustomUser.objects.get(pk=user_pk)
        goods_list = goods_list.filter(user=user).order_by('-created_at')
        words += user.username
    if prefecture_code: # 都道府県検索
        prefecture = Prefectures2.objects.get(code=prefecture_code)
        goods_list = goods_list.filter(user__prefecture=prefecture)
        words += prefecture.name + ' '
    cate1 = Category1.objects.filter(cate1=category1).first()
    if cate1:# 大カテゴリ検索
        goods_list = goods_list.filter(category__cate1=cate1).order_by('-created_at')
        cate2 = Category2.objects.filter(cate1=cate1,cate2=category2).first()
        words += category1
        if cate2:# 詳細カテゴリ検索
            goods_list = goods_list.filter(category=cate2).order_by('-created_at')
            words += ' ' + category2
    if request.GET.get('not_use'):
        goods_list = goods_list.filter(status='新品未使用品').order_by('-created_at')
    if word: # ワード検索
        goods_list = goods_list.filter(Q(name__contains=word) | Q(content__contains=word)).order_by('-created_at')
        words += ' ' + word
    prefecture_form = PrefectureSelectForm(request.POST or None)
    search_num = goods_list.count
    new_time = datetime.datetime.now() - datetime.timedelta(days=1)
    paginator = Paginator(goods_list, 48)
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    context = {
        'category1':category1,
        'category2':category2,
        'word':word,
        'search_num':search_num,
        'new_time':new_time.strftime('%Y-%m-%d-%H'),
        'goods_list': page_obj.object_list,
        'page_obj': page_obj,
        'words': words,
        'prefecture_form': prefecture_form,
    }
    return render(request, 'goods_list.html', context)

# 都道府県別ページ
def prefectures_select(request):
    prefectures = Prefectures2.objects.all()
    context = {
        'prefectures': prefectures,
    }
    return render(request, 'prefecture_list.html', context)

# 都道府県別商品一覧
def prefecture(request, code):
    prefec = Prefectures2.objects.get(code=code)
    goods_list = Goods.objects.filter(user__prefecture=prefec,confirm=True,trade=1).order_by('-created_at')
    new_time = datetime.datetime.now() - datetime.timedelta(days=1)
    context = {
        'new_time':new_time.strftime('%Y-%m-%d-%H'),
        'words': prefec,
        'goods_list': goods_list,
    }
    return render(request, 'goods_list.html', context)

# 新規申し込みか登録してある商品で交換を申し込むか
@login_required
def OfferSelect(request):
    if request.POST:        
        goods = Goods.objects.get(pk=request.POST.get("goods_id"))
        context = {
            'goods': goods,
        }
        if request.POST.get('back1', '') == 's_info':
            context['back1'] = 's_info'
        if request.POST.get('next', '') == 'goods_registed': # 登録済み商品から選択
            my_goods_list = Goods.objects.filter(user=request.user, confirm=True, trade=1).order_by('-created_at')
            context['my_goods_list'] = my_goods_list
            return render(request, 'my_goods_list.html', context)
        if request.POST.get('next', '') == 'confirm': # 登録済み商品を使う
            my_goods = Goods.objects.get(pk=request.POST.get("my_goods_id"))
            if not Offer.objects.filter(goods_registed_id=my_goods.pk, trade=1):
                if request.user.ticket < 1:
                    messages.error(request, "チケットがありません。")
                    context['have_ticket'] = 'none'
            context['my_goods'] = my_goods
            return render(request, 'offer_choice_goods.html', context)
        if request.POST.get('next', '') == 'create': # 登録済み商品を使う
            my_goods = Goods.objects.get(pk=request.POST.get("my_goods_id"))
            if not (goods.trade == 1 and my_goods.trade == 1): # 商品が存在しない場合
                messages.error(request, "相手の商品がありません。")
                return redirect('barter_app:goods_detail',pk=goods.pk) # 商品詳細へ
            context['my_goods'] = my_goods
            if Offer.objects.filter(goods=goods,goods_registed_id=my_goods.pk,trade=1): #すでにもう込み済みの場合
                messages.error(request, "この商品はすでに申し込み済みです")
                my_goods_list = Goods.objects.filter(user=request.user, confirm=True, trade=1)
                context['my_goods_list'] = my_goods_list
                return render(request, 'my_goods_list.html', context)
            if Offer.objects.filter(goods=my_goods,goods_registed_id=goods.pk,trade=1): #すでにもう込み済みの場合
                messages.error(request, "この商品は相手から交換が申し込まれています\n交換申請確認ページからご確認ください")
                my_goods_list = Goods.objects.filter(user=request.user, confirm=True, trade=1)
                context['my_goods_list'] = my_goods_list
                return render(request, 'my_goods_list.html', context)
            if not Offer.objects.filter(goods_registed_id=my_goods.pk, trade=1):
                if request.user.ticket < 1:
                    messages.error(request, "チケットがありません。")
                    return redirect("barter_app:ticket")
                else:
                    user = request.user
                    user.ticket -= 1
                    user.save()
            offer = Offer.objects.create(user=request.user,goods=goods,name=my_goods.name,status=my_goods.status,category=my_goods.category,
                content=my_goods.content, photo1=my_goods.photo1, goods_registed_id=my_goods.pk)

            # 申し込まれたらTodosListに登録
            todos = TodosList.objects.create(offer=offer, user=offer.goods.user, photo1=offer.goods.photo1, dis_flg1=1, dis_flg2=1)
            
            return render(request, 'offer_completion.html', context)
        return render(request, 'offer_select.html', context)
    return render(request, 'offer_select.html')

# 新規商品交換申し込み
@login_required
def GoodsOffer(request):
    if request.POST:
        if not Goods.objects.filter(pk=request.POST.get("goods_id"),trade=1,confirm=True): # 商品が存在するか
            messages.error(request, "相手の商品がありません。")
            return redirect('barter_app:q_a') # 商品が存在しない場合はQ_Aへ
        goods = Goods.objects.get(pk=request.POST.get("goods_id"))
        form = GoodsOfferForm(request.POST or None, request.FILES)
        context = {
            'form': form,
            'goods': goods,
        }
        if request.POST.get('back1', '') == 's_info':
            context['back1'] ='s_info'
        if request.POST.get('next', '') == 'confirm_ticket':
            offer = Offer.objects.get(pk=request.POST.get("offer_id"))
            context = {
                'form': form,
                'goods': goods,
                'offer': offer,
            }
            return render(request, 'offer_confirm.html',context)

        if request.POST.get('next', '') == 'confirm': # 確認画面
            Offer.objects.filter(user=request.user,confirm=False).delete()
            if form.is_valid():
                offer = Offer()
                offer.user = request.user
                offer.name = request.POST.get('name')
                offer.content = request.POST.get('content')
                offer.category =  Category2.objects.get(pk=request.POST.get('category'))
                offer.status = request.POST.get('status')

                photo_list = ['photo1','photo2','photo3','photo4','photo5']
                if request.FILES:
                    for p in photo_list:
                        if request.POST.get(p) == None:
                            if offer.photo1 == None:
                                offer.photo1 = request.FILES[p]
                            elif offer.photo2 == None:
                                offer.photo2 = request.FILES[p]
                            elif offer.photo3 == None:
                                offer.photo3 = request.FILES[p]
                            elif offer.photo4 == None:
                                offer.photo4 = request.FILES[p]
                            elif offer.photo5 == None:
                                offer.photo5 = request.FILES[p]
                offer.confirm = False
                offer.goods = goods
                offer.save()
                context = {
                'form': form,
                'goods': goods,
                'offer': offer,
                }
                if request.POST.get('back1', '') == 's_info':
                    context['back1'] ='s_info'
                if Offer.objects.filter(goods_registed_id=-1, trade=1):
                    if request.user.ticket < 1:
                        messages.error(request, "チケットがありません。")
                        context['have_ticket'] = 'none'
                return render(request, 'offer_confirm.html',context)
            
        if request.POST.get('next', '') == 'back': # 確認画面から入力画面に戻る
            offer = Offer.objects.get(pk=request.POST.get('offer_id'))
            offer.delete()
            return render(request, 'offer_create.html',context)
        if request.POST.get('next', '') == 'create': # 作成完了
            offer = Offer.objects.get(id=request.POST.get('offer_id'))
            offer.confirm = True
            offer.save()
            user = request.user
            user.ticket -= 1
            user.save()
            # 申し込まれたらTodosListに登録
            todos = TodosList(offer=offer, user=offer.goods.user, photo1=offer.goods.photo1, dis_flg1=1, dis_flg2=1)
            todos.save()

            return render(request, 'offer_completion.html', context)
        return render(request, 'offer_create.html', context)
    else:
        return redirect('barter_app:home')

# 自分の交換申し込み一覧
@login_required
def my_offer(request):
    offer = Offer.objects.filter(user=request.user, confirm=True, trade=1).order_by('-created_at') # 自分で申し込んだ
    offer2 = Offer.objects.filter(goods__user=request.user, confirm=True, trade=1).order_by('-created_at') # 申し込まれた

    context = {
        'offer': offer,
        'offer2': offer2,
    }
    
    return render(request, 'my_offer.html', context)

# 申し込み詳細ページ
@login_required
def offer_detail(request):
    offer = Offer.objects.get(pk=request.POST.get('offer_pk'))
    goods = offer.goods
    context = {
        'offer': offer,
        'goods': goods,
    }
    goods2 = None
    if TodosList.objects.filter(Q(offer=offer.pk), Q(offer_flg=0) | Q(todo_flg__isnull=True)):
        todo = TodosList.objects.get(Q(offer=offer.pk), Q(offer_flg=0) | Q(todo_flg__isnull=True))

    if request.POST.get('od1_2', '') == 'todos': # todoリストから画面遷移した場合
        request.session['link2']=request.POST.get('od1_2')
        link2 = request.session['link2']
        context['link2'] = link2
    if request.POST.get('od1_2', '') == 'myoffer': # 交換申請一覧画面から画面遷移した場合
        request.session['link2']=request.POST.get('od1_2')
        link2 = request.session['link2']
        context['link2'] = link2

    if 'link2' in request.session:
        context['link2'] = request.session['link2']

    if offer.goods_registed_id >= 0: # 既存商品から交換申し込みした場合
        if Goods.objects.filter(pk=offer.goods_registed_id):
            goods2 = Goods.objects.get(pk=offer.goods_registed_id)
            context['goods2'] = goods2
    if offer.trade == 2: # 取引成立済みの場合
        trade_his = TradingHistory.objects.get(offer=offer)
        context['trade_date'] = trade_his.completion_at
        return render(request, 'offer_detail.html', context)
    if request.POST.get('next', '') == 'cancel_confirm': # 交換申請取下げ確認画面へ
        return render(request, 'offer_cancel_confirm.html', context)
    if request.POST.get('next', '') == 'cancel': # 交換申請取下げ完了
        if not (goods.trade == 1 and offer.trade == 1): # 商品が存在するか
            messages.error(request, "商品がありません")
            return redirect('barter_app:goods_detail',pk=goods.pk) # 商品が存在しない場合
        user = request.user
        if offer.goods_registed_id == -1: # 新規申し込みだった場合
            user.ticket += 1 # チケットを返却する
            user.save()
        else: # 既存商品からだった場合
            offer_list = Offer.objects.filter(goods_registed_id=offer.goods_registed_id, confirm=True, trade=1)
            if offer_list.count() == 1:
                user.ticket += 1
                user.save()
        todo.offer_flg=1
        todo.dis_flg1=2
        todo.save()
        offer.trade = 5 # 申請取下げ
        offer.save()
        return render(request, 'offer_cancel.html', context)
    if request.POST.get('next', '') == 'trade_confirm': # 交換確認画面へ
        if not Offer.objects.filter(goods_registed_id=goods.pk, trade=1):
            if request.user.ticket < 1:
                messages.error(request, "チケットがありません。")
                context['have_ticket'] = 'none'
        context['confirm'] = 'trade'
        return render(request, 'trade_confirm.html', context)
    if request.POST.get('next', '') == 'refuse_confirm': # 交換拒否確認画面へ
        context['confirm'] = 'refuse'
        return render(request, 'trade_confirm.html', context)
    if request.POST.get('next', '') == 'trade': # 交換する
        if not (goods.trade == 1 and offer.trade == 1): # 商品が存在するか
            if not(offer.trade==1): # オファーが取り下げられている場合
                messages.error(request, "商品への申し込みが取り下げられています。")
                todo.offer_flg = 1
                todo.save()
                return redirect('barter_app:my_offer') # 申し込み一覧へ
            messages.error(request, "商品がありません")
            return redirect('barter_app:goods_detail',pk=goods.pk) # 商品が存在しない場合
        g_user = CustomUser.objects.get(pk=goods.user.pk)
        if not Offer.objects.filter(goods_registed_id=goods.pk,trade=1): #この商品を他の商品の申し込みに出していない場合
            if request.user.ticket < 1:
                messages.error(request, "チケットがありません。")
                return redirect("barter_app:ticket")
            g_user.ticket -= 1 # チケット消費
            g_user.save()
        goods.trade = 2
        goods.save()
        offer.trade = 2
        offer.save()
        todo.todo_flg = 2  # todoリストのtodo_flgを未発送に変更
        todo.offer_flg = 2 # todoリストのoffer_flgを未発送に変更
        todo.save()

        offer_list = Offer.objects.filter(goods_registed_id=goods.pk,trade=1)
        for o in offer_list:
            o.trade = 4 # 出品終了
            o.save()
            if TodosList.objects.filter(offer=o.pk, todoslist_flg=1):
                todo_list = TodosList.objects.filter(offer=o.pk, todoslist_flg=1)
                for t in todo_list:
                    t.dis_flg1 = 2
                    t.dis_flg2 = 2
                    t.save()
        offer_list = Offer.objects.filter(goods=goods, trade=1) # この商品に交換申し込みしている他のオファー
        for o in offer_list:
            if Offer.objects.filter(goods_registed_id=o.goods_registed_id, trade=1).count() == 1: # 交換申し込みが最後の1個だった場合
                o_user = CustomUser.objects.get(pk=o.user.pk)
                o_user.ticket += 1
                o_user.save()
            o.trade = 4
            o.save()
            if TodosList.objects.filter(offer=o.pk, todoslist_flg=1):
                todo_list = TodosList.objects.filter(offer=o.pk, todoslist_flg=1)
                for t in todo_list:
                    t.dis_flg1 = 2
                    t.dis_flg2 = 2
                    t.save()

        if goods2:
            goods2.trade = 2
            goods2.save()
            offer_list = Offer.objects.filter(goods_registed_id=goods2.pk,trade=1)
            for o in offer_list:
                o.trade = 4
                o.save()
            offer_list = Offer.objects.filter(goods=goods2,trade=1)
            for o in offer_list:
                if Offer.objects.filter(goods_registed_id=o.goods_registed_id, trade=1).count() == 1:
                    o_user = CustomUser.objects.get(pk=o.user.pk)
                    o_user.ticket += 1
                    o_user.save()
                o.trade = 4
                o.save()
        comment = goods.user.username + 'の' + goods.name + 'と'  + offer.name + 'の交換が成立しました'
        notice = UserNotice.objects.create(user=offer.user,category=2,comment=comment)
        comment = offer.user.username + 'の' + offer.name + 'と'  + goods.name + 'の交換が成立しました'
        notice2 = UserNotice.objects.create(user=goods.user,category=2,comment=comment)

        trade = TradingHistory.objects.create(goods=goods,offer=offer) # 取引履歴作成
        context['result'] = "trade"
        context['trade'] = trade

        todo.trade = trade # TradingHistoryを登録
        todo.save()

        return render(request, 'trade_completion.html', context)
    if request.POST.get('next', '') == 'refuse': # 交換申し込みを拒否する
        if not (goods.trade == 1 and offer.trade == 1): # 商品が存在するか
            messages.error(request, "商品がありません")
            return redirect('barter_app:goods_detail',pk=goods.pk) # 商品が存在しない場合
        offer.trade = 3
        if Offer.objects.filter(goods_registed_id=offer.goods_registed_id, trade=1).count() == 1:
            o_user = CustomUser.objects.get(pk=offer.user.pk)
            o_user.ticket += 1
            o_user.save()
        offer.save()
        todo.todo_flg = 1
        todo.dis_flg1 = 2
        todo.save()
        comment = goods.user.username + 'の' + goods.name + 'と' + offer.name + 'の交換は不成立でした'
        notice = UserNotice.objects.create(user=offer.user,category=6,comment=comment)

        context['result'] = "refuse"
        return render(request, 'trade_completion.html', context)
    if request.POST.get('next', '') == 'back': # 申請一覧に戻る
        
        return redirect('barter_app:my_offer')
    return render(request, 'offer_detail.html', context)

# 取引中履歴
@login_required
def trading_history(request):
    trade_list = TradingHistory.objects.filter(Q(goods__user=request.user) | Q(offer__user=request.user), Q(status=1)).order_by('-completion_at')

    context = {
        'trade_list': trade_list,
    }
    
    return render(request, 'trading_history.html', context)

# 取引完了履歴
@login_required
def trading_history_end(request):
    trade_list = TradingHistory.objects.filter(Q(goods__user=request.user) | Q(offer__user=request.user), Q(status=2)).order_by('-completion_at')

    context = {
        'trade_list': trade_list,
    }
    
    return render(request, 'trading_history_end.html', context)


# 取引詳細
@login_required
def trading_detail(request):
    trade = TradingHistory.objects.get(pk=request.POST.get('trade_pk'))
    offer = trade.offer
    goods = trade.goods
    todo = TodosList.objects.get(trade=request.POST.get('trade_pk'),todoslist_flg=1)
    if goods.user == request.user:
        my_status = trade.sending1
        other_status = trade.sending2
        sender = 1
    elif offer.user == request.user:
        my_status = trade.sending2
        other_status = trade.sending1
        sender = 2
    context = {
        'my_status':my_status,
        'other_status':other_status,
        'sender': sender,
        'trade': trade,
        'offer': offer,
        'goods': goods,
    }
    if offer.goods_registed_id >= 0: # 既存商品から交換申し込みした場合
        if Goods.objects.filter(pk=offer.goods_registed_id):
            goods2 = Goods.objects.get(pk=offer.goods_registed_id)
            context['goods2'] = goods2

    if request.POST.get('od1_2', ''): # 前の画面のhiddenをsessionに登録
        request.session['link2']=request.POST.get('od1_2')
        link2 = request.session['link2']
        context['link2'] = link2
    if 'link2' in request.session:       # Sessionのlink2にデータが入っていたらcontextに代入
        context['link2'] = request.session['link2']

    if request.POST.get('next', '') == 'ship_confirm': # 発送連絡確認へ
        context['confirm'] = 'ship'
        return render(request, 'ship_confirm.html', context)
    if request.POST.get('next', '') == 'receive_confirm': # 受け取り連絡確認へ
        context['confirm'] = 'receive'
        return render(request, 'ship_confirm.html', context)
    if request.POST.get('next', '') == 'ship': # 発送連絡（確定）
        if sender == 1:
            trade.sending1 = 2
            if todo.todo_flg == 2:
                todo.todo_flg = 3
        if sender == 2:
            trade.sending2 = 2
            if todo.offer_flg == 2:
                todo.offer_flg = 3
        todo.save()
        trade.save()
        context['result'] = "ship"
        return render(request, 'ship_completion.html', context)
    if request.POST.get('next', '') == 'receive': # 受け取り連絡（確定）
        if sender == 1:
            trade.sending1 = 3
            trade.save()
            o_user = offer.user
            if todo.todo_flg == 3:
                todo.todo_flg = 4
                todo.save()
        if sender == 2:
            trade.sending2 = 3
            trade.save()
            o_user = goods.user
            if todo.offer_flg == 3:
                todo.offer_flg = 4
                todo.save()
        context['result'] = "receive"
        form = EvaluationForm(request.POST or None)
        context['form'] = form
        context['o_user'] = o_user
        return render(request, 'ship_completion.html', context)
    return render(request, 'trading_detail.html', context)

# 取引連絡
@login_required
def trading_contact(request):
    if request.method == 'POST':
        trade = TradingHistory.objects.get(pk=request.POST.get('trade_pk'))
        message_all = TradingMessage.objects.filter(trading=trade)
        user_1 = trade.goods.user.pk
        user_2 = trade.offer.user.pk
        if user_1 == request.user.pk:
            my_user = user_1
            partner = user_2
        elif user_2 == request.user.pk:
            my_user = user_2
            partner = user_1
        form = TradingMessageForm(request.POST or None, request.FILES)
        my_list = CustomUser.objects.get(pk=my_user)
        partner_list = CustomUser.objects.get(pk=partner)
        context = {
            'form': form,
            'trade': trade,
            'message_all': message_all,
            'my_list': my_list,
            'partner_list':partner_list,
        }
        if request.POST.get('od1_2', '') == 'todos': # やることリストからきたら
            request.session['link2']=request.POST.get('od1_2')
            link2 = request.session['link2']
            context['link2'] = link2
            todos = TodosList.objects.get(pk=request.POST.get('todo_pk'))
            todos.chat_dis_flg = 2
            todos.save()
        if TodosList.objects.filter(trade=trade.pk,user=my_list, chat_dis_flg=1, chat_gamen=3): # チャット一覧から来てやることリストに通知があったら
            todo = TodosList.objects.get(trade=trade.pk,user=my_list, chat_dis_flg=1, chat_gamen=3)
            todo.chat_dis_flg=2 # 非表示にする
            todo.save()
        if request.POST.get('od1_1', '') == 'todos':
            context['link2'] = 'todos'
    
        if form.is_valid():
            if request.POST.get('next', '') == 'confirm':
                return render(request, 'trading_contact_confirm.html',context)
            if request.POST.get('next', '') == 'back':
                return render(request, 'trading_contact.html',context)
            if request.POST.get('next', '') == 'create': # メッセージ送信
                if message_all:
                    latest_messa = message_all.order_by('-completion_at')[0]
                else:
                    latest_messa = None
                # 同じ送信者が同じメッセージを連投できないようにする
                if latest_messa and latest_messa.message == request.POST.get('message') and latest_messa.sender == request.user.pk:
                    pass
                else:
                    message = form.save(commit=False)
                    message.trading = trade
                    message.sender = my_user
                    message.receiver = partner
                    message.save()
                    comment = my_list.username + 'から取引連絡がきました'
                    notice = UserNotice.objects.create(user=partner_list,category=4,comment=comment)
                    if not TodosList.objects.filter(trade=trade, user=partner_list, user2=int(request.user.pk),todoslist_flg=2, chat_dis_flg=1, chat_gamen=3): # もうすでに通知にあるか、なければ作成
                        todo = TodosList.objects.create(trade=trade, user=partner_list, user2=int(request.user.pk), todoslist_flg=2, chat_dis_flg=1, chat_gamen=3)
                form = ChatMessageForm()
                message_all = TradingMessage.objects.filter(trading=trade)
                context['form'] = form
                context['message_all'] = message_all
                return render(request, 'trading_contact.html',context)
        else:
            return render(request, 'trading_contact.html', context, dict(form=form))

    return render(request, 'trading_contact.html', context, dict(form=form))

# 受け取り評価
@login_required
def user_evaluation(request):
    if request.POST:
        o_user = CustomUser.objects.get(pk=request.POST.get('o_user'))
        trade = TradingHistory.objects.get(pk=request.POST.get('trade_pk'))
        evaluation_list = Evaluation.objects.filter(user=o_user)
        form = EvaluationForm(request.POST or None)
        todo = TodosList.objects.get(trade=request.POST.get('trade_pk'), todoslist_flg=1)
        context = {
            'form': form,
            'trade': trade,
            'o_user': o_user,
            'evaluation_list': evaluation_list,
        }
        if trade.goods.user == request.user and trade.g_eva: #すでに評価済みの場合は戻る
            comp_message = 'すでに評価済みです'
            context['comp_message'] = comp_message
            return render(request, 'evaluation.html', context)
        if trade.offer.user == request.user and trade.o_eva:
            comp_message = 'すでに評価済みです'
            context['comp_message'] = comp_message
            return render(request, 'evaluation.html', context)
        if request.POST.get('next', '') == 'confirm': # 確認画面
            review = request.POST.get('review')
            eva_radio = request.POST.get('radio')
            if eva_radio:
                context['review'] = review
                context['eva_radio'] = eva_radio
                return render(request, 'evaluation_confirm.html', context)
        if request.POST.get('next', '') == 'back': # 戻るボタン
            review = request.POST.get('review')
            eva_radio = request.POST.get('radio')
            if eva_radio:
                context['review'] = review
                context['eva_radio'] = eva_radio
                return render(request, 'evaluation.html', context)
        if request.POST.get('next', '') == 'create': # 評価確定
            if trade.goods.user == request.user:
                trade.g_eva = True
                trade.sending1 = 4
            else:
                trade.o_eva = True
                trade.sending2 = 4
            trade.save()
            review = request.POST.get('review')
            eva_radio = request.POST.get('radio')
            if eva_radio:
                if o_user == todo.user:
                    todo.offer_flg = 5
                    todo.dis_flg2 = 2
                    todo.save()
                else:
                    todo.todo_flg = 5
                    todo.dis_flg1 = 2
                    todo.save()
                eva = Evaluation.objects.create(user=o_user, eva_user=request.user.pk,trading=trade,
                eva_username=request.user.username, review=review, eva_radio=eva_radio)
                if trade.g_eva and trade.o_eva: # 両者とも評価が完了したら
                    trade.status = 2 #　取引完了
                    trade.save()
                    other_eva = Evaluation.objects.filter(trading=trade, confirm=False)
                    for e in other_eva:
                        e.confirm = True
                        if e.eva_radio == 'good':
                            e.user.good_evaluation += 1
                        elif e.eva_radio == 'bad':
                            e.user.bad_evaluation += 1
                        e.user.save()
                        e.save()  
                comment = request.user.username + 'から評価されました'
                notice = UserNotice.objects.create(user=o_user,category=5,comment=comment)
                comp_message = '評価が完了しました'
                context['comp_message'] = comp_message
                return render(request, 'evaluation.html', context)
        return render(request, 'evaluation.html', context)
    else:
        return redirect('barter_app:goods_list')


# 出品一覧(自分の)
class MydisplayView(LoginRequiredMixin, generic.ListView):
    model = Goods
    template_name = 'my_display.html'

    def get(self, request):
        goods = Goods.objects.filter(user=self.request.user).exclude(trade=3).order_by('-created_at')
        paginator = Paginator(goods, 48)
        page = self.request.GET.get('page')
        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        new_time = datetime.datetime.now() - datetime.timedelta(days=1) # 1日前の日時を取得
        context = {
            'new_time':new_time.strftime('%Y-%m-%d-%H'),
            'goods_list': page_obj.object_list,
            'page_obj': page_obj
        }
        return render(self.request, "my_display.html", context)

# チャット機能
@login_required
def chat_message(request):
    if request.POST:
        my_user = int(request.user.pk)
        g_user = int(request.POST.get("g_user")) #チャット相手のユーザーpkを取得
        # 過去にチャットしたことある相手か調べる
        if my_user <= g_user:
            chat_rooms = ChatRoom.objects.filter(user1=my_user, user2=g_user)
        else:
            chat_rooms = ChatRoom.objects.filter(user1=g_user, user2=my_user)

        if chat_rooms: # 既にチャットルームがある場合は取得
            chat_room = chat_rooms.first()
        else: #初めてチャットする相手の場合チャットルームを作成
            if my_user <= g_user: # pkの小さい方をuser1にする
                chat_room = ChatRoom.objects.create(user1=my_user,user2=g_user)
            else:
                chat_room = ChatRoom.objects.create(user1=g_user,user2=my_user)
        
        message_all = ChatMessage.objects.filter(room=chat_room)
        form = ChatMessageForm(request.POST or None)

        my_user_list = CustomUser.objects.get(id = my_user)   # 自分の情報取得
        g_user_list = CustomUser.objects.get(id = g_user)   # 相手の情報取得

        if request.POST.get('link2', '') == 'todos': # やることリストからきたら
            todos= TodosList.objects.get(pk=request.POST.get("todo_pk"))
            todos.chat_dis_flg = 2 # todoリストの通知を非表示にする
            todos.save()
        if request.POST.get('link2', ''):
            request.session['link2'] = request.POST.get('link2')
        if TodosList.objects.filter(user=my_user_list, user2=g_user, chat_dis_flg=1, chat_gamen=1): # チャット一覧から来てやることリストに通知があったら
            todo = TodosList.objects.get(user=my_user_list, user2=g_user, chat_dis_flg=1, chat_gamen=1)
            todo.chat_dis_flg=2 # 非表示にする
            todo.save()
        context = {
            'form': form,
            'my_user':my_user,
            'g_user':g_user,
            'chat_room': chat_room,
            'message_all': message_all,
            'my_list' : my_user_list,
            'partner_list': g_user_list,
            'link2': request.session['link2'],
        }
        if request.POST.get('next', '') == 'send': # 送信した
            if form.is_valid():
                if message_all:
                    latest_messa = message_all.order_by('-created_at')[0]
                else:
                    latest_messa = None
                # 同じ送信者が同じメッセージを連投できないようにする
                if latest_messa and latest_messa.message == request.POST.get('message') and latest_messa.sender == request.user.pk:
                    pass
                else:
                    messa = ChatMessage.objects.create(room=chat_room,sender=my_user, message=request.POST.get('message'))
                    chat_room.last_at = datetime.datetime.now()
                    chat_room.save()
                    comment = my_user_list.username + 'からメッセージがきました'
                    notice = UserNotice.objects.create(user=g_user_list,category=3,comment=comment)
                    if not TodosList.objects.filter(user=g_user_list, user2=my_user,todoslist_flg=2, chat_dis_flg=1, chat_gamen=1):
                        todo = TodosList.objects.create(user=g_user_list,user2=my_user, todoslist_flg=2, chat_dis_flg=1, chat_gamen=1)
                form = ChatMessageForm()
                message_all = ChatMessage.objects.filter(room=chat_room)
                context['message_all'] = message_all
                context['form'] = form
            else:
                messages.error(request, "メッセージを送信できませんでした。")
                return render(request, 'chat_message.html', context)
        if request.POST.get('sgd', ''): # 新しいタブの商品詳細からきた
            context['sgd'] = request.POST.get('sgd')
        if request.POST.get('b_back'): # 一つ前の画面が出品者情報
            context['b_back'] = request.POST.get('b_back')
            if request.POST.get('goods_pk'):
                good_link = Goods.objects.get(pk=request.POST.get('goods_pk'))
                context['good_link'] = good_link
            if request.POST.get('o_goods_pk'):
                o_good_link = Offer.objects.get(pk=request.POST.get('o_goods_pk'))
                context['o_good_link'] = o_good_link
                context['nex'] = request.POST.get('nex')
        if request.POST.get('offer_pk',''):
            offer = Offer.objects.get(pk=request.POST.get('offer_pk'))
            context['offer'] = offer
        if request.POST.get('trade_pk', ''):
            trade = TradingHistory.objects.get(pk=request.POST.get('trade_pk'))
            context['trade'] = trade
        if request.POST.get('goods_pk'):
            good_link = Goods.objects.get(pk=request.POST.get('goods_pk'))
            context['good_link'] = good_link
        if request.POST.get('back1', ''): # 新しいタブの出品者情報からきた場合
            context['back1'] = request.POST.get('back1')
            return render(request, 'chat_message.html', context)
        if 'link2' in request.session:
            link2 = request.session['link2']
            if link2 == 'g_detail':
                goods = Goods.objects.get(pk=request.POST.get('goods_pk'))
                context['goods'] = goods

        return render(request, 'chat_message.html', context)
    return redirect('barter_app:chat_room_list')

# チャット一覧
@login_required
def chat_room_list(request):
    my_user = request.user.pk
    room_list = ChatRoom.objects.filter(Q(user1=my_user) | Q(user2=my_user), is_active=True).order_by('-last_at')
    room_dict = {}
    for r in room_list:
        if r.user1 == my_user:
            if CustomUser.objects.filter(pk=r.user2).exists():
                c_user = CustomUser.objects.get(pk=r.user2)
                room_dict[r] = c_user
        else:
            if CustomUser.objects.filter(pk=r.user1).exists():
                c_user = CustomUser.objects.get(pk=r.user1)
                room_dict[r] = c_user

    context = {
        'room_dict': room_dict,
    }
    return render(request, 'chat_room_list.html', context)

# マイページ　パスワード変更
User = get_user_model()
def change_password(request):
    """パスワード変更"""

    user = User.objects.get(id=request.user.id)
    if request.method == 'GET':
        form = ChangePasswordForm(user_id=request.user.id)
    elif request.method == 'POST':
        form = ChangePasswordForm(user_id=request.user.id, data=request.POST)

        if form.is_valid():
            confirm_new_password = form.cleaned_data['confirm_new_password']
            user.set_password(confirm_new_password)
            user.save()
            user = authenticate(
                username=user.username,
                password=confirm_new_password,)
                
            if user is not None:
                login(request, user)
            else:
                raise RuntimeError('invalid')
            return HttpResponseRedirect(reverse('barter_app:change_success'))
    context = {
        'form': form,
    }
    return render(request, 'change_password.html', context)
    
class change_successView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'change_success.html'

# やることリスト
@login_required
def TodosView(request):
    todos_user = TodosList.objects.filter(user=request.user, dis_flg1=1, todoslist_flg=1) # 出品者が自分のtodoリスト
    todos_offer = TodosList.objects.filter(trade__offer__user=request.user, dis_flg2=1, todoslist_flg=1) # 申請者が自分のtodoリスト
    todos_offer2 = TodosList.objects.filter(offer__user=request.user,todo_flg=1, dis_flg2=1, todoslist_flg=1) # 申請した商品が拒否されたtodoリスト
    todos_offer3 = TodosList.objects.filter(offer__user=request.user,todo_flg=6, dis_flg2=1, todoslist_flg=1) # 申請した商品が削除されたtodoリスト
    todos_chat = TodosList.objects.filter(user=request.user, todoslist_flg=2, chat_dis_flg=1) # チャットの相手が自分のtodoリスト

    todos_list = sorted(chain(todos_user, todos_offer, todos_offer2, todos_offer3, todos_chat), key=attrgetter('created_at')) # 上の2つリストに入れて日付順に並べた
    # やることリストに出力する言葉
    text =[
        "の交換申請がきました。",
        "申請していた商品が削除されました。",
        "の交換申請を拒否されました。",
        "の取引が成立しました。商品を発送してください。",
        "が届いて内容を確認したら、受取連絡と相手の評価をしてください。",
        "の取引が完了しました。相手の評価をしてください。",
        "メッセージがきました。",
        "にコメントがきました。",
    ]

    context = {
        'todos_list': todos_list,
        'text': text,
    }
    return render(request, 'todos.html', context)

# やることリストの拒否と削除の通知を消す
@login_required
def tododelview(request, pk):
    todo = get_object_or_404(TodosList, pk=pk)
    todo.dis_flg2=2
    todo.save()
    return redirect('barter_app:todos')

# お知らせ一覧
@login_required
def user_notice(request):
    my_user = request.user.pk
    notice_list = UserNotice.objects.filter(user=my_user).order_by('-created_at')
    
    context ={
        'notice_list': notice_list,
    }

    return render(request, 'notice_list.html', context)

# ユーザー一覧
@login_required
def user_list(request):
    user_list = CustomUser.objects.all().exclude(pk=request.user.pk)
    
    context = {
        'user_list': user_list,
    }
    return render(request, 'user_list.html', context)

#プロフィール(出品者情報)
@login_required
def seller_info(request):
    if request.POST:
        g_user = int(request.POST.get("g_user")) #チャット相手のユーザーpkを取得
        form = ChatMessageForm(request.POST or None)
        g_user_list = CustomUser.objects.get(id = g_user)# 相手の情報取得
        goods = Goods.objects.filter(user=g_user_list,confirm=True,trade=1).order_by('-created_at')
        new_time = datetime.datetime.now() - datetime.timedelta(days=1)
        context = {
            'new_time':new_time.strftime('%Y-%m-%d-%H'),
            'form': form,
            'g_user':g_user,
            'partner_list': g_user_list,
            'goods_list': goods,
        }
        if request.POST.get('goods_pk'):
            good_link = Goods.objects.get(pk=request.POST.get('goods_pk'))
            context['good_link'] = good_link
        if request.POST.get('o_goods_pk'):
            o_good_link = Offer.objects.get(pk=request.POST.get('o_goods_pk'))
            context['o_good_link'] = o_good_link

        if request.POST.get('back1', '') == 's_info': # 出品者情報の商品リンクを押した場合
            context['back1'] = 's_info'
        if request.POST.get('nex', '') == 'goods_detail':
            context['nex'] = 'goods_detail'
        if 'link' in request.session: # もしsessionにlinkのkeyのデータがあった場合
            link = request.session['link']
            context['link'] = link            
            if link == 'o_detail':
                offer = Offer.objects.get(pk=request.POST.get('offer_pk'))
                context['offer'] = offer
            if link == 't_detail':
                trade = TradingHistory.objects.get(pk=request.POST.get('trade_pk'))
                context['trade'] = trade
        if 'link2' in request.session: # もしsessionにlink2のkeyのデータがあった場合
            context['link2'] = request.session['link2']

        return render(request, 'seller_information.html', context)
    return redirect('barter_app:goods_detail')

# 過去の取引事例一覧
@login_required
def transaction_example(request):
    trade_list = TradingHistory.objects.all().order_by('-completion_at')
    context = {
        'trade_list': trade_list,
    }
    return render(request, 'transaction_example.html', context)

# 過去取引詳細
@login_required
def transaction_detail(request,pk):
    trade = TradingHistory.objects.get(pk=pk)
    context = {
        'trade': trade,
    }
    return render(request, 'transaction_detail.html', context)

# ユーザー評価一覧
@login_required
def user_evaluation_list(request, pk):
    g_user = CustomUser.objects.get(pk=pk)
    evaluation_list = Evaluation.objects.filter(user=g_user, confirm=True).order_by('-created_at')
    good_evaluation = evaluation_list.filter(eva_radio='good').order_by('-created_at')
    bad_evaluation = evaluation_list.filter(eva_radio='bad').order_by('-created_at')
    paginator = Paginator(evaluation_list, 48)
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    context = {
        'g_user':g_user,
        'evaluation_list': page_obj.object_list,
        'good_evaluation': good_evaluation,
        'bad_evaluation': bad_evaluation,
        'page_obj': page_obj,
    }
    return render(request, 'evaluation_list.html', context)

# 過去の成立取引検索
def trade_search(request):
    trade_list = TradingHistory.objects.all().order_by('-completion_at')
    goods_list = Goods.objects.filter(confirm=True,trade=1).order_by('-created_at')
    user_pk = request.GET.get('user_pk')
    category1 = request.GET.get('category1')
    category2 = request.GET.get('category2')
    word = request.GET.get('word')
    words = ''
    if user_pk: # ユーザー別商品
        user = CustomUser.objects.get(pk=user_pk)
        goods_list = goods_list.filter(user=user).order_by('-created_at')
        trade_list = trade_list.filter(Q(goods__user=user) | Q(offer__user=user)).order_by('-completion_at')
        words += user.username
    cate1 = Category1.objects.filter(cate1=category1).first()
    if cate1:# 大カテゴリ検索
        goods_list = goods_list.filter(category__cate1=cate1).order_by('-created_at')
        trade_list = trade_list.filter(Q(goods__category__cate1=cate1) | Q(offer__category__cate1=cate1)).order_by('-completion_at')
        cate2 = Category2.objects.filter(cate1=cate1,cate2=category2).first()
        words += category1
        if cate2:# 詳細カテゴリ検索
            goods_list = goods_list.filter(category=cate2).order_by('-created_at')
            trade_list = trade_list.filter(Q(goods__category=cate2) | Q(offer__category=cate2)).order_by('-completion_at')
            words += ' ' + category2
    if request.GET.get('not_use'):
        goods_list = goods_list.filter(status='新品未使用品').order_by('-created_at')
    if word: # ワード検索
        goods_list = goods_list.filter(Q(name__contains=word) | Q(content__contains=word)).order_by('-created_at')
        trade_list = trade_list.filter(Q(goods__name__contains=word) | Q(goods__content__contains=word) | Q(offer__name__contains=word) | Q(offer__content__contains=word)).order_by('-completion_at')
        words += ' ' + word
    context = {
        'trade_list': trade_list,
        'words': words,
    }
    return render(request, 'transaction_example.html', context)

# 住所印刷
@login_required
def address_print(request):
    if request.POST:
        user = CustomUser.objects.get(pk=request.user.pk)
        trade = TradingHistory.objects.get(pk=request.POST.get('trade_pk'))
        context = {
            'user' : user,
            'trade': trade,
        }
        return render(request, 'address_print.html', context)
    else:
        return redirect('barter_app:trading_history')

# 商品詳細
def seller_goods_detail(request, pk):
    goods = Goods.objects.get(pk=pk)
    comments = GoodsComment.objects.filter(goods=goods)
    form = GoodsCommentForm(request.POST or None)
    if request.user.pk: # ログイン中
        send_user = request.user
        exist_fav_user = FavUser.objects.filter(user=send_user, item_id=goods)
        is_exist_fav = exist_fav_user.exists()
    else: # ログインしていない場合
        is_exist_fav = False
    context = {
        'object':goods,
        'form': form,
        'comments':comments,
        'is_exist_fav':is_exist_fav,
    }
    if request.POST: # コメント投稿
        #if not request.user.pk:
        #    return redirect('account:login')
        if form.is_valid():
            latest_come = GoodsComment.objects.filter(goods=goods,user=request.user).order_by('-created_at') # 同じコメントを連投した場合
            if latest_come:
                if latest_come[0].comment == request.POST.get('comment'):
                    context['form'] = GoodsCommentForm()
                    return render(request, 'goods_detail.html', context)
            comment = form.save(commit=False)
            comment.goods = goods
            comment.user = request.user
            comment.save()
            user1 = int(request.user.pk)
            context['form'] = GoodsCommentForm()
            if goods.user != request.user:
                if not TodosList.objects.filter(goods=goods,todoslist_flg=2,chat_dis_flg=1,chat_gamen=2): # todoリストに通知がなければ表示で登録
                    todos = TodosList.objects.create(goods=goods, user=goods.user, user2=user1,todoslist_flg=2,chat_dis_flg=1,chat_gamen=2)
                if TodosList.objects.filter(goods=goods,todoslist_flg=2,chat_dis_flg=1,chat_gamen=2): # todoリストに通知があれば非表示で登録
                    todos = TodosList.objects.create(goods=goods, user=goods.user, user2=user1,todoslist_flg=2,chat_dis_flg=2,chat_gamen=2)
            return render(request, 'seller_goods_detail.html', context)
        else:
            return render(request, 'seller_goods_detail.html', context)
    return render(request, 'seller_goods_detail.html', context)