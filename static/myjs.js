/* homeの検索のところ　
　　大カテゴリーを選択すると小カテゴリーが選択できるようにした */

var $category = $('#category2');
var original = $category.html();

$('#category1').change(function() {

    var val1 = $(this).val();

    $category.html(original).find('option').each(function() {
        var val2 = $(this).data('val');

        if (val1 != val2) {
            $(this).not(':first-child').remove();
        }
    });

    if ($(this).val() == "") {
        $category.attr('disabled', 'disabled');
    }else {
        $category.removeAttr('disabled');
    }
});

/*
function popupImage() {
    var popup = document.getElementById('js-popup');
    if(!popup) return;
  
    var blackBg = document.getElementById('js-black-bg');
    var closeBtn = document.getElementById('js-close-btn');
    var showBtn = document.getElementById('js-show-popup');
  
    closePopUp(blackBg);
    closePopUp(closeBtn);
    closePopUp(showBtn);
    function closePopUp(elem) {
      if(!elem) return;
      elem.addEventListener('click', function() {
        popup.classList.toggle('is-show');
      });
    }
  }
  popupImage();*/


  const images = [
    "https://data.whicdn.com/images/251238216/original.gif",
    "https://i.pinimg.com/originals/3d/22/45/3d224511a5e13317e46e37bee1d249dd.gif",
    "https://i.pinimg.com/originals/b0/10/e9/b010e954f94acbd034917b2d6931bd79.gif",
    "https://64.media.tumblr.com/5f4c0252b15dda55028536c5a8923c7d/b691a90722d7bbb5-c8/s500x750/34aac55dfd7302e41fec400ba9636edeadb1890a.gif",
    "https://64.media.tumblr.com/746e848c8b0cf90bc7938599421e6b4e/tumblr_pbhfk0rEth1txe8seo1_500.gif",
    "https://media2.giphy.com/media/Wm9XlKG2xIMiVcH4CP/giphy.gif"
  ];
  
  $(document).ready(function () {
    let current = 0;
    $("#mirror-content").on("click", function () {
      $(this).css({
        "background-image": `url(${images[++current % images.length]})`
      });
    });
  });
  